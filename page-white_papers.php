<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: White Papers
 */

get_header();

global $post;
$custom = get_post_custom($post->ID);
$large_image = $custom["large_image"][0];
$large_url = $custom["large_url"][0];
$button_text = $custom["large_url"][0];
$color_scheme = $custom["color_scheme"][0];
$small_image = $custom["small_image"][0]; ?>

<section id="slider_nh"> <img src="<?php echo $small_image; ?>" style="margin-left:25px;" /> </section>
<section id="content_mid" class="small_hero">
<div id="container_mid">
  <div id="content" role="main">
    <section id="main_content">
      <h1>WHITE PAPERS</h1>
      <p>Our white papers provide marketers from around the world the insights they need to engage their social community.</p>

      <p>Social marketing success is within your reach with our detailed research. Covering a wide variety of topics, our goal is to deliver marketers and brands the information they need to succeed in all of their social marketing campaigns.</p>

      <div class="whitepapers"><img class="aligncenter size-full wp-image-556" title="white_paper" src="http://www.vitrue.com/wp-content/uploads/2011/12/WP_New_Temp_soc_games.jpg" alt="" width="186" height="152" />
      <h3>How Brands Should Use Social Gaming</h3>
      <p>Learn insights and best practices on how to develop a branded social game.</p>
      <a target="_blank" data-paper="social_games"><img class="alignnone size-full wp-image-581 download" title="download" src="http://www.vitrue.com/wp-content/uploads/2011/07/download.png" alt="" width="119" height="29" /></a>
      </div>

      <div class="whitepapers"><img class="aligncenter size-full wp-image-556" title="white_paper" src="http://www.vitrue.com/wp-content/uploads/2011/12/WP_Old_Temp_social_mobile.jpg" alt="" width="186" height="152" />
      <h3>Social Mobile User Engagement</h3>
      <p>Learn proven strategies, insightful data and best practices for mobile social user engagement.</p>
      <a target="_blank" data-paper="mobile_wp"><img class="alignnone size-full wp-image-581 download" title="download" src="http://www.vitrue.com/wp-content/uploads/2011/07/download.png" alt="" width="119" height="29" /></a>
      </div>

      <div class="whitepapers"><img class="aligncenter size-full wp-image-556" title="white_paper" src="http://www.vitrue.com/wp-content/uploads/2011/11/White_PapersIMAGE1.png" alt="" width="186" height="152" />
      <h3>The Guest that Never Leaves</h3>
      <p>Learn How to Protect Your Social Community from Unwanted Content Through Effective Community Moderation Tactics</p>
      <a title="Spam Vs Your Brand" href="" target="_blank" data-paper="spam_v_brand"><img class="alignnone size-full wp-image-581 download" title="download" src="http://www.vitrue.com/wp-content/uploads/2011/07/download.png" alt="" width="119" height="29" /></a>
      </div>

      <div class="whitepapers"><img class="aligncenter size-full wp-image-556" title="white_paper" src="http://www.vitrue.com/wp-content/uploads/2011/11/White_Papers-1_05.png" alt="" width="186" height="152" />
      <h3>Managing Your Facebook Community</h3>
      <p>Find Out the Best Ways to Manage Your Social Community on Facebook. Detailed Research on Conversation Volume, Post Engagement and More</p>
      <a title="Managing Your Facebook Community" href="" target="_blank" data-paper="managing_communities"><img class="alignnone size-full wp-image-581 download" title="download" src="http://www.vitrue.com/wp-content/uploads/2011/07/download.png" alt="" width="119" height="29" /></a>
      </div>

      <div class="whitepapers"><img class="aligncenter size-full wp-image-556" title="white_paper" src="http://www.vitrue.com/wp-content/uploads/2011/11/White_Papers-1_07.png" alt="" width="186" height="152" />
      <h3>Anatomy of a Facebook Post</h3>
      <p>Curious When is the Best Time to Engage With Your Facebook Community? Find Out the Best Times, Frequency, Post Types and More</p>
      <a target="_blank" data-paper="anatomy_of_fb_wp"><img class="alignnone size-full wp-image-581 download" title="download" src="http://www.vitrue.com/wp-content/uploads/2011/07/download.png" alt="" width="119" height="29" /></a>
      </div>
        
    </section>
    <?php get_sidebar('product'); ?>
    <div class="clear"></div>
  </div><!-- #content --> 
  <span id="container_end"></span> 
</div><!-- #container -->

<?php get_footer(); ?>
