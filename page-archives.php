<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Archive Index
 *
 */

get_header(); ?>

<style type="text/css">
.nh ul li{list-style-image:none !important;}
</style>

	<div id="slider_nh"></div>
      <section id="content_mid" class="nh">
		<div id="container_mid"> 
			<div id="content" role="main">
				<section id="main_content">

	<h1>Archive Index</h1>
    <div class="clear"></div>
			<?php the_post(); ?>
		<div style="width:50%;float:left;">	
		<h2>Archives by Month:</h2>
        
		<ul>
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
		</div>
        <div style="width:50%;float:left;">
        
		<h2>Archives by Category:</h2>
		<ul>
			 <?php wp_list_categories('title_li='); ?>
		</ul>
		</div>
        <div class="clear"></div>
		</section>
                <?php get_sidebar('product'); ?>
				<div class="clear"></div>
			</div><!-- #content -->
            <span id="container_end"></span>
		</div><!-- #container -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
