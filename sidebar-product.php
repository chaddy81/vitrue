<?php
/**
 * The Sidebar for Product pages.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */
?>

<aside id="sidebar">
    <a href="http://vitrue.com/request-a-demo" class="demo">sign up for a<br /><span>live demo<img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></span></a>
    <p id="fans_served">social connections <br /> 
    	<span class="large_grey">
    	<?php
    		$curl_handle=curl_init();
    		curl_setopt($curl_handle,CURLOPT_URL,'http://vitruefancounter.dev1.atl.vitrue.com/total');
    		curl_exec($curl_handle);
    		curl_close($curl_handle); 

    	?>
    	</span>
    </p>
    <div>
    <h3 style="margin-bottom:15px;">COMMUNITY MANAGER OF THE YEAR AWARD</h3>
    <img src="http://www.vitrue.com/wp-content/uploads/2012/02/CMY_side_image.png" />
    <p>Nominate your community manager rockstar for the 2012 Community Manager of the Year award. Finalists win a free trip to the WoMMA Summit in Vegas!</p>
    <a href="http://www.vitrue.com/community-manager-of-the-year" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;">LEARN MORE HERE AND NOMINATE <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-3px;"></a>
    </div>
    <div class="divider"></div>
    <p>
    <?php if(is_page('white-papers')){ ?>
		<h3 style="margin:15px 0;">Recent Tweets</h3>
        <ul id="jstwitter">
         	
        </ul>
        <img src="<?php bloginfo('template_directory'); ?>/images/icon_follow.png" style="vertical-align:-3px;" /><a href="http://www.twitter.com/vitrue" style="font-weight:600;font-size:11px;margin-left:5px;">FOLLOW US <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px;" /></a>
    <?php }else{ ?>
    <h3>white papers</h3>
    <div class="whitepaper">
        <p class="title">How Brands Should Use Social Gaming</p>
        <p>Learn insights and best practices on how to develop a branded social game.</p>
        <a href="http://vitrue.com/education/white-papers" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/download.png" style="vertical-align:-1px;" /> DOWNLOAD <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-2px;">
    </div>
    <div class="divider"></div>
    <div class="whitepaper">
        <p class="title">Social Mobile User Engagement</p>
        <p>Learn proven strategies, insightful data and best practices for mobile social user engagement.</p>
        <a href="http://vitrue.com/education/white-papers" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/download.png" style="vertical-align:-1px;" /> DOWNLOAD <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-2px;">
    </div>
    <div class="divider"></div>
    <div class="whitepaper">
        <p class="title">The Guest that Never Leaves</p>
        <p>Learn How to Protect Your Social Community from Unwanted Content.</p>
        <a href="http://vitrue.com/education/white-papers" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/download.png" style="vertical-align:-1px;" /> DOWNLOAD <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-2px;">
    </div>
    <!-- <div class="divider"></div>
    <div class="whitepaper">
        <p class="title">Managing Your Facebook Community</p>
        <p>Find Out the Best Ways to Manage Your Social Community on Facebook.</p>
        <a href="http://vitrue.com/education/white-papers" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/download.png" style="vertical-align:-1px;" /> DOWNLOAD <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-2px;">
    </div>
    <div class="divider"></div>
    <div class="whitepaper">
        <p class="title">Anatomy of a Facebook Post</p>
        <p>Curious When is the Best Time to Engage With Your Facebook Community?</p>
        <a href="http://vitrue.com/education/white-papers" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/download.png" style="vertical-align:-1px;" /> DOWNLOAD <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-2px;">
    </div> -->
    <?php } ?>
</aside>