<?php
/**
 * The Sidebar for Vitrue TV pages.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */
 
  global $post;
  $custom = get_post_custom($post->ID);
  $event_date = $custom["event_date"];
  $event_location = $custom["event_location"];
    
?>

<aside id="sidebar">
    <a href="http://vitrue.com/request-a-demo" class="demo">sign up for a<br /><span>live demo<img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></span></a>
    <p id="fans_served">social connections <br /> 
			<span class="large_grey">
			<?php
				$curl_handle=curl_init();
				curl_setopt($curl_handle,CURLOPT_URL,'http://vitruefancounter.dev1.atl.vitrue.com/total');
				curl_exec($curl_handle);
				curl_close($curl_handle);

			?>
			</span>
		    </p>
   <div>
    <h3 style="margin-bottom:15px;">COMMUNITY MANAGER OF THE YEAR AWARD</h3>
    <img src="http://www.vitrue.com/wp-content/uploads/2012/02/CMY_side_image.png" />
    <p>Nominate your community manager rockstar for the 2012 Community Manager of the Year award. Finalists win a free trip to the WoMMA Summit in Vegas!</p>
    <a href="http://www.vitrue.com/community-manager-of-the-year" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;">LEARN MORE HERE AND NOMINATE <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-3px;"></a>
    </div>
    <div class="divider"></div>
    <h2>Recent Awards</h2>
    
    <h4 style="text-transform:none;margin-bottom:10px;"><a href="http://www.forbes.com/lists/2011/28/most-promising-companies-11_rank.html">Forbes’ Most Promising Companies</a></h4>
    
    <h4 style="text-transform:none">Business to Business Magazine</h4>
    <p>Reggie Bradford: Top 25 Entrepreneurs</p>
    
    <h4 style="text-transform:none">TiE Atlanta</h4>
    <p>Reggie Bradford: Entrepreneur of the Year Finalist</p>
    
    <h4 style="text-transform:none;margin-bottom:20px;">Red Herring: Global 100 Winner</h4>
    
	  <div class="divider"></div>
    <h2>Events Calendar</h2>
    <?php
	  $args = array( 'post_type' => 'event-calendar', 'posts_per_page' => 10, 'orderby' => 'date' );
	  $loop = new WP_Query( $args );
	 
	  while ( $loop->have_posts() ) : $loop->the_post();
      	global $post;
        $custom = get_post_custom($post->ID);
        $event_date = $custom["event_date"][0];
        $event_location = $custom["event_location"][0]; ?>
      	<div class='event'>
		  <?php echo the_content(); ?>
      <?php  
   		  echo "<span style='display:block' id='location'>$event_location</span>";
   		  echo "<span style='display:block' id='date'>$event_date</span>";	  
		    echo "</div>";
	  endwhile;
	   ?>
    <div class="divider"></div>
    <h2>Media Kit</h2>
    <a href="<?php bloginfo('template_directory'); ?>/vitrue_logo.zip" style="display:block;margin-bottom:5px;"><img src="<?php bloginfo('template_directory'); ?>/images/icon_download.png" style="vertical-align:-2px;margin-right:3px;" />Download Company Logo <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px" /></a>
    <a href="<?php bloginfo('template_directory'); ?>/Product_Screen_Grabs.zip" style="display:block;margin-bottom:5px;"><img src="<?php bloginfo('template_directory'); ?>/images/icon_download.png" style="vertical-align:-2px;margin-right:3px;" />Download Product Screens <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px" /></a>
    <a href="<?php bloginfo('template_directory'); ?>/CompanyFactSheet.pdf" style="display:block;margin-bottom:5px;"><img src="<?php bloginfo('template_directory'); ?>/images/icon_download.png" style="vertical-align:-2px;margin-right:3px;" />Download Company Fact Sheet <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px" /></a>
   
</aside>
