<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Product
 */

get_header(); 


global $post;
$custom = get_post_custom($post->ID);
$large_image = $custom["large_image"][0];
$large_url = $custom["large_url"][0];
$button_text = $custom["large_url"][0];
$color_scheme = $custom["color_scheme"][0];
        
?>

	  <section id="slider">
          <img src="<?php echo $large_image; ?>" class="product_image" />
      </section>
      <section id="content_mid" class="product">
		<div id="container_mid">
        
			<div id="content" role="main">
            	<section id="main_content">
                  <?php
					/* Run the loop to output the page.
					 * If you want to overload this in a child theme then include a file
					 * called loop-page.php and that will be used instead.
					 */
					get_template_part( 'loop', 'page' );
				  ?>
                  
				</section>
                <?php get_sidebar('product'); ?>
				<div class="clear"></div>
			</div><!-- #content -->
            <span id="container_end"></span>
		</div><!-- #container -->
        

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
