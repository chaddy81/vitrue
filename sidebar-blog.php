<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */
?>

<aside id="sidebar">
    <a href="http://vitrue.com/request-a-demo" class="demo">sign up for a<br /><span>live demo<img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></span></a>
    <p id="fans_served">social connections <br /> 
		<span class="large_grey">
			<?php
				$curl_handle=curl_init();
				curl_setopt($curl_handle,CURLOPT_URL,'http://vitruefancounter.dev1.atl.vitrue.com/total');
				curl_exec($curl_handle);
				curl_close($curl_handle);

			?>
			</span>
	    </p>
    
   <?php $pr = get_category_by_slug( 'press-releases' ); 
   		 $press_release = $pr->term_id; 
		 
		 $pc = get_category_by_slug( 'press-coverage' ); 
   		 $press_coverage = $pc->term_id;
		 
		 $uc = get_category_by_slug( 'uncategorized' ); 
   		 $uncategorized = $uc->term_id;
		 
		 //wp_list_categories('number=8&exclude='.$press_release.', '.$press_coverage); ?>
		<div>
    <h3 style="margin-bottom:15px;">COMMUNITY MANAGER OF THE YEAR AWARD</h3>
    <img src="http://www.vitrue.com/wp-content/uploads/2012/02/CMY_side_image.png" />
    <p>Nominate your community manager rockstar for the 2012 Community Manager of the Year award. Finalists win a free trip to the WoMMA Summit in Vegas!</p>
    <a href="http://www.vitrue.com/community-manager-of-the-year" style="color:#52799e !important;font-weight:500;font-size:11.5px;margin-left:0;font-family:'ubuntu';text-shadow:0 0 1px #cccdd0;">LEARN MORE HERE AND NOMINATE <img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/arrow.png" style="vertical-align:-3px;"></a>
    </div>
	    <div class="divider"></div>

		 <h3 style="margin:15px 0;">Top Categories</h3>
		 <a href="http://www.vitrue.com/category/facebook/" class="blog-cat">Facebook</a>
		 <a href="http://www.vitrue.com/category/twitter/" class="blog-cat">Twitter</a>
		 <a href="http://www.vitrue.com/category/google-plus/" class="blog-cat">Google +</a>
		 <a href="http://www.vitrue.com/category/facebook-best-practices-from-vitrue/" class="blog-cat">Facebook Best Practices</a>
		 <a href="http://www.vitrue.com/category/social-by-the-numbers/" class="blog-cat">Social By The Numbers</a>
		 <a href="http://www.vitrue.com/category/vitrue-social-relationship-manager/" class="blog-cat">Vitrue SRM</a>
		 <a href="http://www.vitrue.com/category/live-events/" class="blog-cat">Live Events</a>
		 <a href="http://www.vitrue.com/category/industry-news/" class="blog-cat">Industry News</a>
		 <p></p>
         <img src="<?php bloginfo('template_directory'); ?>/images/icon_categories.png" style="vertical-align:-3px;" /><a href="http://vitrue.com/archives/" style="font-weight:600;font-size:11px;margin-left:5px;">MORE CATEGORIES <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px;" /></a>
         <div class="divider"></div>
         <h3 style="margin:15px 0;">Recent Tweets</h3>
         <ul id="jstwitter">
         	
         </ul>
         <img src="<?php bloginfo('template_directory'); ?>/images/icon_follow.png" style="vertical-align:-3px;" /><a href="http://www.twitter.com/vitrue" style="font-weight:600;font-size:11px;margin-left:5px;">FOLLOW US <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px;" /></a>
         <div class="divider"></div>	 
		 <h3 style="margin:15px 0;">Archives</h3>
		 <ul id="archives">
		 	<?php wp_get_archives('limit=8&type=monthly'); ?>
         </ul> 
         <img src="<?php bloginfo('template_directory'); ?>/images/icon_archives.png" style="vertical-align:-3px;" /><a href="http://vitrue.com/archives/" style="font-weight:600;font-size:11px;margin-left:7px;">MORE ARCHIVES <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" style="vertical-align:-3px;" /></a>  
</aside>		
