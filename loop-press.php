<?php
/**
 *
 * @package WordPress
 * @subpackage 
 * @since Vitrue 3.0
 */
?>

<h2 style="margin-top:10px !important;margin-bottom:10px;display:inline-block;">Press Releases</h2>
<ul style="display:inline-block;">
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2012">2012</a></li>
	<li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2011">2011</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2010">2010</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2009">2009</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2008">2008</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2007">2007</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-releases?y=2006">2006</a></li>
</ul>
<?php
$args = array( 'category_name' => 'Press Releases' );
$lastposts = get_posts( $args );
foreach($lastposts as $post) : setup_postdata($post); ?>
	<div style="margin-bottom:10px;" class="post">
        <p style="margin-bottom:5px !important;"><span style="font-weight:600;font-size:14px;"><a href="<?php the_permalink(); ?>" style="text-decoration:none; color:#4d4a42;"><?php the_title(); ?></a></span>
        <span class="meta-sep">|</span>
        <span class="entry-meta" style="color:#4d4a42;">
            <?php the_time('F jS, Y') ?> 
        </span><!-- .entry-meta -->
        </p>
	<?php the_excerpt(); ?>
    </div>
<?php endforeach; ?>

<h2 style="margin-top:10px !important;margin-bottom:10px;display:inline-block;">Press Coverage</h2>
<ul style="display:inline-block;">
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2012">2012</a></li>
	<li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2011">2011</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2010">2010</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2009">2009</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2008">2008</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2007">2007</a></li>
    <li style="display:inline;list-style-image:none;padding:0 5px;"><a href="http://www.vitrue.com/category/press-coverage?y=2006">2006</a></li>
</ul>
<?php rewind_posts(); ?>
<?php
$args = array( 'category_name' => 'Press Coverage' );
$lastposts = get_posts( $args );
foreach($lastposts as $post) : setup_postdata($post); ?>
	<div class="press_coverage"><?php the_content(); ?></div>
<?php endforeach; ?>