<?php
/**
 * The Sidebar for Vitrue TV pages.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */
?>

<aside id="sidebar">
   <h2>Scheduled Downtime</h2>
   <p>Vitrue will have downtime for maintenance <b>every Thursday night from 11pm to 4am EST Friday morning</b>. The Vitrue SRM platform, including all products, may be inaccessible during this time.</p>

   <h2>Subscribe</h2>
   <p>Receive updates directly to your inbox.</p>
   <form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=vitrue/mHAp', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true"><p>Enter your email address:</p><p><input type="text" style="width:200px" name="email"/></p><input type="hidden" value="vitrue/mHAp" name="uri"/><input type="hidden" name="loc" value="en_US"/><input type="submit" value="Subscribe" /></form>

   <h2>Featured Posts</h2>
   <?php 
     query_posts( 'product=featured&posts_per_page=2' );
     if(have_posts()) : while (have_posts()) : the_post();
      $post_thumbnail_id = get_post_thumbnail_id();
      $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );

    if($post_thumbnail_url){ ?>
      <div style="margin-bottom:15px;">
        <img src="<?php echo bloginfo('template_directory'); ?>/scripts/timthumb.php?src=<?php echo $post_thumbnail_url; ?>&h=70&w=225" />
        <?php } 
         the_title('<h4 style="color:#595959;">', '</h4>');
         //the_excerpt();
         $excerpt = get_the_excerpt();
         $exc = substr_replace($excerpt, '...', 100, -1);
         echo substr($exc, 0, -1); ?>
         <br />
         <p><a href="<?php the_permalink(); ?>">Read More</a></p>
      </div>
  <?php
     endwhile;
    endif;
   ?>
   
   <h2>Quick Links</h2>
   <a href="http://www.vitrue.com/product/product-publisher/" style="display:block;">Publisher</a>
   <a href="http://www.vitrue.com/product/product-tabs/" style="display:block;">Tabs</a>
   <a href="http://www.vitrue.com/product/product-analytics/" style="display:block;">Analytics</a>
   <a href="http://www.vitrue.com/product/product-admin/" style="display:block;">SRM Administration</a>
   <a href="http://www.vitrue.com/product/product-shop/" style="display:block;">Shop</a>

   <h2>Help Files</h2>
   <p>We've created hundreds of help articles to guide you through each part of the Vitrue SRM platform. The articles are organized by product, so you can easily find the exact info you need.</p> 
   <p><a href="http://help.vitrue.com/kb/">Knowledge Base</a></p>

   <h2>Training</h2>
   <p>We host free webinar training every Thursday for our various products. This training can either help guide you through getting started with the Vitrue SRM Platform, or give you in-depth info on specific products within SRM.</p> 
   <p><a href="http://help.vitrue.com/kb/training-videos/webinar-training-schedule">Training Schedule</a></p>

   <h2>Support</h2>
   <p>The Vitrue Support team is available 24/7 to answer questions or take feedback.</p> 
   <p>Email Support: <a href="mailto:support@vitrue.com">support@vitrue.com</a></p>
</aside>