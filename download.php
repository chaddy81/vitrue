<?php
$file = bloginfo('template_directory') . '/white-papers/SPAM-WP.pdf';

function download ($file){

header('Pragma: public');             // required

header('Expires: 0');                       // to prevent caching

header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Cache-Control: private',false);

header('Content-Type: application/force-download');

header('Content-Disposition: attachment; filename="'.basename($file).'"');

header('Content-Transfer-Encoding: binary');

header('Connection: close');

readfile($file);                   // push it out

exit();

}
?>