<?php
/**
 * The main template file.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */

get_header(); ?>

	<div id="slider_nh">
		<img src="http://vitrue.com/wp-content/uploads/2011/08/blogHero_03.png" style="margin-left:30px;" />
	</div>
	<section id="content_mid" class="blog">
	<div id="container_mid">
		<div id="content" role="main">
	    	<section id="main_content">
	        <h1>Blog</h1>
	 
	          <?php
				/* Run the loop to output the page.
				 * If you want to overload this in a child theme then include a file
				 * called loop-page.php and that will be used instead.
				 */
				 $pr = get_category_by_slug( 'press-releases' ); 
				 $press_release = $pr->term_id; 
				 
				 $pc = get_category_by_slug( 'press-coverage' ); 
				 $press_coverage = $pc->term_id;
	 
				 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; 
				 query_posts('cat= -' . $press_release . ', -' . $press_coverage . '&paged=' . $paged ."&orderby=date&order=DESC");
				 get_template_part( 'loop', 'index' );
			  ?>
	          
			</section>
	        <?php get_sidebar('blog'); ?>
			<div class="clear"></div>
		</div><!-- #content -->
	    <span id="container_end"></span>
	</div><!-- #container -->

<?php get_footer(); ?>