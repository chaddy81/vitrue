<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Secondary
 */

get_header(); 

global $post;
$custom = get_post_custom($post->ID);
$large_image = $custom["large_image"][0];
$large_url = $custom["large_url"][0];
$button_text = $custom["large_url"][0];
$color_scheme = $custom["color_scheme"][0];
$small_image = $custom["small_image"][0];
?>
  <section id="slider">
	<img src="<?php echo $small_image; ?>" style="margin-top:35px;" />
  </section>
  <section id="content_mid" class="secondary" style="margin-bottom:110px;">
	<div id="container_mid">
		<div id="content" role="main">
        	<section id="main_content">
            	<h1>Platform OVERVIEW</h1>
                <p class="bold">The Vitrue Social Relationship Management (SRM) Platform Makes Brands Social.</p>
                <p>Vitrue gives brands and agencies the tools and technology they need to execute their social marketing strategies, and our Social Relationship Management (SRM) platform is the leading social marketing solution for global brands and marketers.</p>

                <p>Marketers are faced with the question of whether or not to include social in their marketing campaigns.  Beyond that, they have to decide how it fits in with their other multichannel campaigns. Can social really drive leads and sales? How much time and manpower will it take to manage my social marketing campaigns? Can I maintain control over our brand and message?  Vitrue has your answers.</p>
			</section>
            <aside id="sidebar">
            	<a href="vitrue.com/request-a-demo" class="demo">sign up for a <br /> <span>live demo</span><img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></a>
                <p id="fans_served">social connections <br /> 
        			<span class="large_grey">
        			<?php
        				$curl_handle=curl_init();
        				curl_setopt($curl_handle,CURLOPT_URL,'http://vitruefancounter.dev1.atl.vitrue.com/total');
        				curl_exec($curl_handle);
        				curl_close($curl_handle);

        			?>
        			</span>
	            </p>
            </aside>
            <div class="clear"></div>
            <div id="platforms">
            	<div class="cont_1">
                	<img src="http://vitrue.com/wp-content/uploads/2011/08/platform_pub.png" />
                    <h3>Vitrue Publisher</h3>
                    <p>Social Media Publishing for Facebook, Twitter and Google+.</p>
                    <a href="http://vitrue.com/?page_id=32" class="demo">learn more<img src="<?php bloginfo('template_directory'); ?>/images/white_arrow.png" /></a>
                </div>
                <div class="cont_2">
                	<img src="http://vitrue.com/wp-content/uploads/2011/08/platform_tabs.png" />
                    <h3>Vitrue Tabs</h3>
                    <p>Custom Facebook Modules Featuring Dynamic, Engaging Content.</p>
                    <a href="http://vitrue.com/?page_id=39" class="demo">learn more<img src="<?php bloginfo('template_directory'); ?>/images/white_arrow.png" /></a>
                </div>
                <div class="cont_3">
                	<img src="http://www.vitrue.com/wp-content/uploads/2011/11/platform_shop_new.png" />
                    <h3>Vitrue Shop</h3>
                    <p>Social Commerce Solutions For Your Brand.</p>
                    <a href="http://vitrue.com/?page_id=42" class="demo">learn more<img src="<?php bloginfo('template_directory'); ?>/images/white_arrow.png" /></a>
                </div>
                <div class="cont_4">
                	<img src="http://www.vitrue.com/wp-content/uploads/2011/11/platform_analytics_new.png" />
                    <h3>Vitrue Analytics</h3>
                    <p>The Social Community Analytics Marketers Need.</p>
                    <a href="http://vitrue.com/?page_id=44" class="demo">learn more<img src="<?php bloginfo('template_directory'); ?>/images/white_arrow.png" /></a>
                </div>
                <div class="clear"></div>

            </div>
            <p style="padding:0 50px;">Our SRM platform is a software-as-a-service (SaaS) technology that allows marketers to publish and engage fans and customize a brand's look, feel and message. It can be used for marketing, daily customer service, brand awareness, social commerce, to mirror marketing campaigns, and for research. It's also easy to use, scalable, efficient, and quite simply is the right technology for the demanding 24/7 job of social marketing.</p>
            <section id="cont_pane">
            	<h3>The Vitrue SRM Platform is perfect for:</h3>
            	<ul>
                    <li>Brand marketers who want to grow their brand awareness and engage with their social community</li>
                    <li>Agencies that provide strategy, media, creative and technology assessment solutions to brands wanting to expand their social presence</li>
                    <li>Medium sized companies that are looking to grow their social presence and need a partner that can help with technology and strategy</li>
                    <li>Business owners that need a one-stop social marketing platform</li>
                </ul>
            </section>
            <section id="cont_pane">
            	<h3>With the Vitrue SRM platform, you get a streamlined social marketing account management tool, including:</h3>
            	<ul>
                    <li><strong>Universal Navigation:</strong> Our Dashboard encompasses all Vitrue products and 	provides marketers with the analytics and data they need to make decisions</li>
                    <li><strong>Single Sign-On:</strong> A single log-in that allows access to all products 	protected by advanced security protocols </li>
                    <li><strong>Stream 	Level Management:</strong>  Organize people and products into teams, allowing 	different access levels to different teams ‚ you choose who has 	access to what product</li>
		            <li><strong>Single Dashboard:</strong> Allowing you to manage your Twitter, Facebook and Google+ communities within one platform</li>
                </ul>
            </section>
            <div class="clear"></div>
    		<section id="cont_pane">
    			<h3>How Can You Know Vitrue is the Right Choice?</h3>
    			<ul>
    			  <li><strong>Tested and Proven:</strong> Vitrue helps brands and agencies more efficiently and successfully managing its clients’ almost 815 million collective social connections in 47 countries across 3,000 Facebook, Twitter and Google+ accounts.</li>
    			  <li><strong>Insiders:</strong> Vitrue is an original Facebook Preferred Developer Group member and Google+ Beta Trial Participant, giving us insider-level knowledge allowing us to quickly evolve products to compliment Facebook and Google+ platform changes as they happen. </li>
    			  <li><strong>Raving Fans:</strong> Vitrue's top-tier client references span from F100 to F3000 global companies.  We have longstanding relationships with the world's most innovative brands.</li>
    			  <li><strong>Thought Leadership:</strong> Vitrue provides clients with a competitive advantage through our insights into industry trends and social media best practices via our white papers, case studies, video and blog content.</li>
    			</ul>
    		</section>
    		<section id="cont_pane">
    		    <img src="http://vitrue.com/wp-content/uploads/2011/07/tabs_graphic_2.jpg" style="width:400px;" />
    		</section>
            <div class="clear"></div>
			
		</div><!-- #content -->
        <!--<div class="clear"></div>-->
        <span id="container_end"></span>
	</div><!-- #container -->
        
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
