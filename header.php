<?php
header('Cache-Control: max-age=28800');
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till content section.
 *
 * @package Vitrue 3.0
 * @subpackage none
 *
 */
?>
<?php
if(!ob_start("ob_gzhandler")) ob_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<!--<meta property="og:title" content="Vitrue"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.vitrue.com"/>-->
<meta property="og:image" content="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/fb_logo.jpg"/>
<!--<meta property="og:site_name" content="Vitrue"/>-->
<meta property="og:description" content="We make brands social."/>
<title><?php wp_title(''); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/colorbox.css" />
<link href='http://fonts.googleapis.com/css?family=Cabin:400,500,600,700,400italic,500italic,600italic,700italic&v2' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic&v2' rel='stylesheet' type='text/css'>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

<!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="selectivizr.js"></script>
  <noscript><link rel="stylesheet" href="[fallback css]" /></noscript>
<![endif]-->

<!--[if IE 8]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>

<style type="text/css">
	#menu-primary li a {font-size:12px;padding:10px 8px 14px 10px;}
    #sidebar a.demo {background:none !important;filter:none;}
    #resources_cont nav a {font-size:12px;}
</style>
<![endif]-->

<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" media="all" href="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	
	wp_head();
?>

</head>

<body>

<script type="text/javascript">

 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-2685777-1']);
 _gaq.push(['_trackPageview']);

 (function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();

</script>

<section id="container">
 <?php if(!is_page_template('page-product-blog.php') && (!is_singular( 'product-blog' )) && (!is_tax('product'))){ ?>
<div id="connect_tab">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=155197021264936";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <p>CONNECT:</p>
  <div class="fb-like" data-href="http://www.facebook.com/vitrue" data-send="false" data-layout="box_count" data-width="70" data-show-faces="false"></div>
  <g:plusone size="tall" id="gplus_add"></g:plusone>
	<script src='https://apis.google.com/js/plusone.js'></script>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	<a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical">Tweet</a>
	<a href="https://twitter.com/vitrue" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @vitrue</a>
	<!-- <iframe allowtransparency="true" frameborder="0" scrolling="no" style="margin-top:0px;width:62px;height:20px;"
	src="//platform.twitter.com/widgets/follow_button.html?screen_name=vitrue&show_count=false&show_screen_name=false"></iframe> -->
	<!-- <a href="http://pinterest.com/pin/create/button/?url=http://www.vitrue.com/&media=http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/Vitrue_Pin.png" class="pin-it-button" count-layout="vertical" style="margin-top:-8px;margin-left:5px !important">Pin It</a> -->
	<a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.vitrue.com&media=http%3A%2F%2Fwww.vitrue.com%2Fwp-content%2Fthemes%2FVitrue-3.0%2Fimages%2FVitrue_Pin.png&description=Vitrue" class="pin-it-button" count-layout="vertical"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
	<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
</div>
<?php } ?>
<header> 
	<?php if(is_page_template('page-product-blog.php') || (is_singular( 'product-blog' )) || (is_tax('product'))){ ?>
		<span class="logo"><a href="http://www.vitrue.com/product-updates/"><h1>Vitrue</h1></a></span>
	<?php }else{ ?>
		<span class="logo"><a href="<?php bloginfo('home'); ?>"><h1>Vitrue</h1></a></span>
	<?php } ?>
	<section id="cta"> 
 		<a href="http://vitrue.com/request-a-demo" id="demo">Sign Up For a Demo</a> 
        <a href="https://accounts.vitrue.com/" onClick="s.prop10='D=pageName'; s.linkTrackVars='prop10,events'; s.linkTrackEvents='event9'; s.events = s.apl(s.events,'event9',',',2); s.tl(this,'o','Login Click');" id="log_in">Log In</a> 
    </section>
  	<?php if(!is_page_template('page-product-blog.php') && (!is_singular( 'product-blog' )) && (!is_tax('product'))){ ?>
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'depth' => 3 ) ); ?>
	<?php } ?>
	<section id="hdr_nav"> 
    <?php wp_nav_menu( array( 'container' => 'nav', 'theme_location' => 'hdr-menu', 'depth' => 1) ); ?>
    <div id="search">
      <?php get_search_form(); ?>
    </div>
  </section>
  <?php //if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
</header>