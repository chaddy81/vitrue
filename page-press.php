<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Press
 */

get_header(); 
        
?>

<div id="slider_nh">
	  <img src="http://vitrue.com/wp-content/uploads/2011/08/Press.png" />
</div>
<section id="content_mid" class="small_hero">
<div id="container_mid">
	<div id="content" role="main">
    	<section id="main_content">
        
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        	<?php the_content(); ?>
        <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
        
          <?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'press' );
		  ?>
          
		</section>
        <?php get_sidebar('pr'); ?>
		<div class="clear"></div>
	</div><!-- #content -->
    <span id="container_end"></span>
</div><!-- #container -->

<?php get_footer(); ?>
