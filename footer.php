<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */
?>

<div class="clear"></div>
</section>
</section>
<footer>
  <div id="nav_cont">
    <?php wp_nav_menu( array( 'container' => 'nav', 'container_id' => 'footer_nav', 'theme_location' => 'footer-menu') ); ?>
    <span id="connect">Connect with us: <a href="http://www.facebook.com/vitrue" class="fb"></a><a href="http://www.twitter.com/vitrue" class="tw"></a><a href="https://plus.google.com/u/3/b/109542470558012974022/" class="gplus"></a></span> </div>
  <p id="copyright"> All contents &copy; 2012 Vitrue. All rights reserved. </p>
</footer>
<div id="modal">
  <div id="close_cont"><span class="close" style="margin-left:775px;font-weight:bold;">X</span></div>
  <div id="modal_cont"></div>
</div>
<div id="modal2">
  <div id="close_cont"><span class="close"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/close1.png" /></span></div>
  <div id="modal_cont"></div>
</div>
<?php

global $post;
$custom = get_post_custom($post->ID);
$google = $custom["google"][0];
$omniture = $custom["omniture"][0];
$manticore = $custom["manticore"][0];
$dfa = $custom["dfa"][0];
$x1 = $custom["x1"][0];


echo $google;
echo $omniture;
echo $manticore;
echo $dfa;
echo $x1; ?>
<?php
global $post;

$template_file = get_post_meta($post->ID,'_wp_page_template',TRUE);

	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */ ?>
<script>
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
  
function exec_pinmarklet() {
    var e=document.createElement('script');
    e.setAttribute('type','text/javascript');
    e.setAttribute('charset','UTF-8');
    e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r=' + Math.random()*99999999);
    document.body.appendChild(e);
	};
</script>
<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/LAB.min.js"></script>
<script>
$LAB
	.script('<?php bloginfo('template_directory'); ?>/js/slides.jquery.js')
	.script('<?php bloginfo('template_directory'); ?>/js/jquery.easing.js')
	.script('<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js')
	.script('<?php bloginfo('template_directory'); ?>/js/jPaginate.js')
	.script('<?php bloginfo('template_directory'); ?>/js/twitter.js')
	.script('<?php bloginfo('template_directory'); ?>/js/jquery.ba-hashchange.min.js').wait()
	.script('<?php bloginfo('template_directory'); ?>/js/vitrue-scripts.js')
	.script('https://apis.google.com/js/plusone.js');
</script>

<script type="text/javascript">
 $(document).ready(function(){
   $('div.whitepapers a').click(function(event){
	  event.preventDefault();
	  var wp = $(this).attr('data-paper');
	  _gaq.push(['_trackEvent', 'WhitePapers', 'Download - step 1', wp]);
	  make_overlay();
	  $('#modal').css('display', 'block');
	  $('#modal #modal_cont').load("<?php bloginfo('template_directory'); ?>/ajax/" + wp + ".php").attr('data-paper', wp);		
  });

  $('#resources_cont div.paper a').click(function(event){
	  event.preventDefault();
	  var wp = $(this).attr('data-paper');
	  _gaq.push(['_trackEvent', 'WhitePapers', 'Download - step 1', wp]);
	  make_overlay();
	  $('#modal').css('display', 'block');
	  $('#modal #modal_cont').load("<?php bloginfo('template_directory'); ?>/ajax/" + wp + ".php").attr('data-paper', wp);		
  });

  $('#resources_cont div.kit a').click(function(event){
	  event.preventDefault();
	  var wp = $(this).attr('data-paper');
	  _gaq.push(['_trackEvent', 'Kit', 'Download - step 1', wp]);
	  make_overlay();
	  $('#modal').css('display', 'block');
	  $('#modal #modal_cont').load("<?php bloginfo('template_directory'); ?>/ajax/" + wp + ".php").attr('data-paper', wp);		
  });

  $('a#test').click(function(event){
	  event.preventDefault();
	  make_overlay();
	  $('#modal').css({'display':'block', 'top':'350px', 'left':'50%', 'margin-left':'-300px'});
	  $('#modal #modal_cont').load("http://www.vitrue.com/live-web-training/");
  });
  
  var make_overlay = function(){
	  var x = $(window).width();
	  var y = $(document).height();
	  $('body').append('<div id="overlay"></div>');
	  $('#overlay').width(x);
	  $('#overlay').height(y);
  }
  
  $('span.close').click(function(){
	  $('#modal').css('display', 'none');
	  $('#overlay').width(0);
	  $('#overlay').height(0);
  });
 });
  
</script>


<?php include_once('tracking/omniture.php'); ?>
 
<!--Begin Manticore Technology Code--> 
<!--Copyright 2000-2011, Manticore Technology Corporation.  All rights reserved.  Patent pending.--> 
<!--Consumer Privacy Statement available at www.ManticoreTechnology.com/ConsumerPrivacy.asp--> 
<!--www.ManticoreTechnology.com--> 

<script language="javascript" type="text/javascript">
	var MTC_GROUP="609";
	var MTC_ID="14977";
	var MTC_Key="F5EA631F-7C5F-4A3E-8F18-074E65CD9B90";
</script> 
<script language="JavaScript">var MTCjsv;</script> 
<script language="JavaScript1.0">MTCjsv="1.0";</script> 
<script language="JavaScript1.1">MTCjsv="1.1";</script> 
<script language="JavaScript1.2">MTCjsv="1.2";</script> 
<script language="JavaScript1.3">MTCjsv="1.3";</script> 
<script language="JavaScript1.4">MTCjsv="1.4";</script> 
<script language="JavaScript1.5">MTCjsv="1.5";</script> 
<script language="JavaScript1.6">MTCjsv="1.6";</script> 
<script language="JavaScript1.7">MTCjsv="1.7";</script> 
<script language="JavaScript" src="/mtc/mtcJSAPI.js"></script> 

<script language="javascript" type="text/javascript">
	mtcGO();
</script> 
<!--End Manticore Technology Code--> 

<?php
	wp_footer();
?>
</body>
</html>