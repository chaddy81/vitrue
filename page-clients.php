<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Our Clients
 */

get_header(); ?>
<style type="text/css">
#container{
  padding-bottom: 125px; }
</style>

<section id="slider_nh">
	<img src="http://vitrue.com/wp-content/uploads/2011/08/Who-Weve-Helped1.png" />
</section>
<section id="content_mid" class="secondary">
<div id="container_mid">
	<div id="content" role="main">
    	<section id="main_content">
        	<h1>WHO WE'VE HELPED</h1>
			<p style="font-size:13px !important; line-height:19px !important;">What do we mean when we say "We Make Brands Social"?  It means we know how to make social media work for everyone, from the marketer at a global brand that needs to target different locations with different messages, to brand executives looking for an enterprise solution, and to medium and small business owners who are still growing their social media presence. Vitrue proudly works with market-leading brands, agencies and business owners - below are some examples of our growing list of incredible customers.</p>

			<?php
				/* Run the loop to output the page.
				 * If you want to overload this in a child theme then include a file
				 * called loop-page.php and that will be used instead.
				 */
				get_template_part( 'loop', 'clients' );
			 ?>
         
		</section>
        <aside id="sidebar">
        	<a class="demo">sign up for a <br /> <span>live demo</span><img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></a>
            <p id="fans_served">social connections <br /> <span class="large_grey">
				<?php
					$curl_handle=curl_init();
					curl_setopt($curl_handle,CURLOPT_URL,'http://vitruefancounter.dev1.atl.vitrue.com/total');
					curl_exec($curl_handle);
					curl_close($curl_handle); 
				?>
			</span></p>
        </aside>
		<div class="clear"></div>
	</div><!-- #content -->
    <span id="container_end"></span>
</div><!-- #container -->
        
<?php get_footer(); ?>
