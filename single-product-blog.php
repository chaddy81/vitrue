<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */

get_header(); ?>

	  <div id="slider_nh"></div>
      <section id="content_mid" class="nh">
		<div id="container_mid">
        
			<div id="content" role="main">
            	<section id="main_content">
                  <?php
					/* Run the loop to output the page.
					 * If you want to overload this in a child theme then include a file
					 * called loop-page.php and that will be used instead.
					 */
					get_template_part( 'loop', 'product-blog' );
				  ?>
                  <!--<div id="share">
                  	<span>share: <a href="#" class="share_fb"></a><a href="#" class="share_tw"></a><a href="#" class="share_email"></a></span>
                  </div>-->
				</section>
                <?php get_sidebar('product-blog'); ?>
				<div class="clear"></div>
			</div><!-- #content -->
            <span id="container_end"></span>
		</div><!-- #container -->
        

<?php //get_sidebar(); ?>
<?php get_footer(); ?>