(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
  $('input#mant_submit').click(function() {
    return $.ajax({
      url: 'http://vitrue.com/wp-content/themes/Vitrue-3.0/process.php',
      data: $('#mant_form').serialize(),
      success: __bind(function() {
        $('#modal').html('<p>Thanks</p>');
        return popWindow();
      }, this),
      error: function() {
        return alert('error');
      }
    });
  });
}).call(this);
