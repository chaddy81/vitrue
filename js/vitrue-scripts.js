$(document).ready(function(){
	
	$("#slides").slides({
	  play: 5000,
	  hoverPause: true,
	  pause: 100,
	  preload: true,
	  effect: 'slide',
	  slideEasing: "easeOutExpo",
	  slideSpeed: 1000
  });
  
  $("#news").slides({
	  play: 7000,
	  hoverPause: true,
	  generatePagination: false,
	  pause: 100,
	  effect: 'fade',
	  slideEasing: "easeInOutQuad",
	  slideSpeed: 1500,
	  container: 'news_container'
  });
	
	$(window).hashchange(function() {
		var hash = window.location.hash.slice(1);
		var height = parseInt(hash) + 75;
		$('#content').css('height', height +'px');
		$('#aria_frame').css('height', height +'px');
		$(document).scrollTop(0);
		//alert(">>onHashchange, height " + height);
	});

   if(window.location.hash){ 
	  var hash = window.location.hash.slice(1);
	  var height = parseInt(hash) + 75;
	  $('#content').css('height', height +'px');
	  $('#aria_frame').css('height', height +'px');
	  $(document).scrollTop(0);
   }
	  
  $("#menu-primary ul.sub-menu").each(function(){
	  $(this).siblings('a').before('<a class="arrow"></a>');
  });
   
  $("#menu-primary > li").hover(function(){
	  $(this).children('ul:first').css('display', 'block');
	  $(this).children('a').addClass('active');
  }, function(){
	  $(this).children('ul:first').css('display', 'none');
	  $(this).children('a').removeClass('active');
  });
  
  $('td.calendar a#next').click(function(event){
	  event.preventDefault();
	  var page = $(this).attr('href');
	  alert(page);
	  $('#modal #modal_cont').load(page);
  });
  
  $("#videos").jPaginate({items: 9});
  $(".open_vid").colorbox({iframe: true, height: 400, width: 640});
  $(".open_vid2").colorbox({iframe: true, height: 400, width: 640});

  $('.analytics_image img').click(function(){
		var url = $(this).attr('data-url');
		var title = $(this).attr('title');
		var docTop = $(document).scrollTop() + 50;
		var imgWidth, imgLeft, closeLeft;
		var t = document.createElement('img');
		t.src = url;

		$("<img/>").attr("src", t.src).load(function() {
			imgWidth = this.width;
		});
		
		if(imgWidth > 0){
			imgLeft = (imgWidth/2) - imgWidth;
		}else{
			imgLeft = -417;
		}
		
		make_overlay2();
		$('#modal2 #modal_cont').append('<img src="'+url+'" /><h1>'+title+'</h1>');
		$('#modal2').css({'margin-left' : imgLeft});
		$('#modal2').css({'display':'block', 'top': docTop});		

		$('#modal2 #close_cont').css({'right' : -15});
	});

	var make_overlay2 = function(){
		var x = $(window).width();
		var y = $(document).height();
		$('body').append('<div id="overlay2"></div>');
		$('#overlay2').width(x);
		$('#overlay2').height(y);
	}

	$('#modal2 #close_cont').click(function(){
	  $('#modal2').css('display', 'none');
	  $('#overlay2').remove();
	  $('#modal2 #modal_cont img').remove();
	  $('#modal2 #modal_cont h1').remove();
	});

	$('#resources_cont nav a').click(function(event){
		event.preventDefault();
		var imageUrl = "http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/ed_res_nav.png";
		var clickedTab = $(this).attr('data-role');
		
		$('#resources_cont .tab_cont').hide();
		$('#resources_cont nav a').removeClass('active');
		$('#resources_cont nav a[data-role='+clickedTab+']').addClass('active');
		$('#resources_cont .tab_cont#'+clickedTab).css('display', 'block');
	});

	var name = "#connect_tab";
	var menuYloc = null;

	menuYloc = parseInt($(name).css("top").substring(0,$(name).css("top").indexOf("px")))
	$(window).scroll(function () { 
		offset = menuYloc+$(document).scrollTop()+"px";
		$(name).animate({top:offset},{duration:500,queue:false});
	});

});