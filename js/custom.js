
	
	$(window).hashchange(function() {
		var hash = window.location.hash.slice(1);
		var height = parseInt(hash) + 75;
		$('#content').css('height', height +'px');
		$('#aria_frame').css('height', height +'px');
		$(document).scrollTop(0);
		//alert(">>onHashchange, height " + height);
	});

   if(window.location.hash){ 
	  var hash = window.location.hash.slice(1);
	  var height = parseInt(hash) + 75;
	  $('#content').css('height', height +'px');
	  $('#aria_frame').css('height', height +'px');
	  $(document).scrollTop(0);
   }
	  
  $("#menu-primary ul.sub-menu").each(function(){
	  $(this).siblings('a').before('<a class="arrow"></a>');
  });
   
  $("#menu-primary > li").hover(function(){
	  $(this).children('ul:first').css('display', 'block');
	  $(this).children('a').addClass('active');
  }, function(){
	  $(this).children('ul:first').css('display', 'none');
	  $(this).children('a').removeClass('active');
  });
		  
  $("#slides").slides({
	  play: 5000,
	  hoverPause: true,
	  pause: 100,
	  preload: true,
	  effect: 'slide',
	  slideEasing: "easeOutExpo",
	  slideSpeed: 1000
  });
  
  $("#news").slides({
	  play: 7000,
	  hoverPause: true,
	  generatePagination: false,
	  pause: 100,
	  effect: 'fade',
	  slideEasing: "easeInOutQuad",
	  slideSpeed: 1500,
	  container: 'news_container'
  });
  
  $('div.whitepapers a').click(function(event){
	  event.preventDefault();
	  make_overlay();
	  $('#modal').css('display', 'block');
	  var wp = $(this).attr('data-paper');
	  $('#modal #modal_cont').load("http://www.vitrue.com/wp-content/themes/Vitrue-3.0/ajax/" + wp + ".php").attr('data-paper', wp);		
  });
  
  var make_overlay = function(){
	  var x = $(window).width();
	  var y = $(document).height();
	  $('body').append('<div id="overlay"></div>');
	  $('#overlay').width(x);
	  $('#overlay').height(y);
  }
  
  $('span.close').click(function(){
	  $('#modal').css('display', 'none');
	  $('#overlay').width(0);
	  $('#overlay').height(0);
  });
  
	$("#videos").jPaginate({items: 9});
	$(".open_vid").colorbox({iframe: true, height: 400, width: 640});
	$(".open_vid2").colorbox({iframe: true, height: 400, width: 640}); 
