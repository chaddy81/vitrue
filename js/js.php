<?php
  if(extension_loaded('zlib')){
    ob_start('ob_gzhandler');
  }
  header ('content-type: text/javascript; charset: UTF-8');
  header ('cache-control: must-revalidate');
  $offset = 60 * 60;
  $expire = 'expires: ' . gmdate ('D, d M Y H:i:s', time() + $offset) . ' GMT';
  header ($expire);
  ob_start('compress');
  function compress($buffer) {
      // remove comments 
      $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
      return $buffer;
    }
 
   // list CSS files to be included
    	include('slides.min.jquery.js');
	include('jquery.easing.js');
	include('jquery.colorbox-min.js');
	include('jPaginate.js');
	include('twitter.js');
	include('jquery.ba-hashchange.min.js');
	include('custom.js');
 
  if(extension_loaded('zlib')){ob_end_flush();}
?>