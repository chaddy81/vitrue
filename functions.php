<?php
/**
 * TwentyTen functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, twentyten_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'twentyten_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */

function mytheme_tinymce_config( $init ) {
 $valid_iframe = 'iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]';
 if ( isset( $init['extended_valid_elements'] ) ) {
  $init['extended_valid_elements'] .= ',' . $valid_iframe;
 } else {
  $init['extended_valid_elements'] = $valid_iframe;
 }
 return $init;
}
add_filter('tiny_mce_before_init', 'mytheme_tinymce_config');

add_action( 'after_setup_theme', 'twentyten_setup' );

if ( ! function_exists( 'twentyten_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override twentyten_setup() in a child theme, add your own twentyten_setup to your child theme's
 * functions.php file.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 * @uses add_custom_background() To add support for a custom background.
 * @uses add_editor_style() To style the visual editor.
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_custom_image_header() To add support for a custom header.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
      
	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'twentyten', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'twentyten' ),
	) );

	// This theme allows users to set a custom background
	add_custom_background();

	// Your changeable header business starts here
	if ( ! defined( 'HEADER_TEXTCOLOR' ) )
		define( 'HEADER_TEXTCOLOR', '' );

	// No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
	if ( ! defined( 'HEADER_IMAGE' ) )
		define( 'HEADER_IMAGE', '%s/images/headers/path.jpg' );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to twentyten_header_image_width and twentyten_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyten_header_image_width', 940 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyten_header_image_height', 198 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 940 pixels wide by 198 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Don't support text inside the header image.
	if ( ! defined( 'NO_HEADER_TEXT' ) )
		define( 'NO_HEADER_TEXT', true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See twentyten_admin_header_style(), below.
	add_custom_image_header( '', 'twentyten_admin_header_style' );

	// ... and thus ends the changeable header business.

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
		'berries' => array(
			'url' => '%s/images/headers/berries.jpg',
			'thumbnail_url' => '%s/images/headers/berries-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Berries', 'twentyten' )
		),
		'cherryblossom' => array(
			'url' => '%s/images/headers/cherryblossoms.jpg',
			'thumbnail_url' => '%s/images/headers/cherryblossoms-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Cherry Blossoms', 'twentyten' )
		),
		'concave' => array(
			'url' => '%s/images/headers/concave.jpg',
			'thumbnail_url' => '%s/images/headers/concave-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Concave', 'twentyten' )
		),
		'fern' => array(
			'url' => '%s/images/headers/fern.jpg',
			'thumbnail_url' => '%s/images/headers/fern-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Fern', 'twentyten' )
		),
		'forestfloor' => array(
			'url' => '%s/images/headers/forestfloor.jpg',
			'thumbnail_url' => '%s/images/headers/forestfloor-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Forest Floor', 'twentyten' )
		),
		'inkwell' => array(
			'url' => '%s/images/headers/inkwell.jpg',
			'thumbnail_url' => '%s/images/headers/inkwell-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Inkwell', 'twentyten' )
		),
		'path' => array(
			'url' => '%s/images/headers/path.jpg',
			'thumbnail_url' => '%s/images/headers/path-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Path', 'twentyten' )
		),
		'sunset' => array(
			'url' => '%s/images/headers/sunset.jpg',
			'thumbnail_url' => '%s/images/headers/sunset-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Sunset', 'twentyten' )
		)
	) );
}
endif;

if ( ! function_exists( 'twentyten_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in twentyten_setup().
 *
 * @since Twenty Ten 1.0
 */
function twentyten_admin_header_style() {
?>
<style type="text/css">
/* Shows the same border as on front end */
#headimg {
	border-bottom: 1px solid #000;
	border-top: 4px solid #000;
}
/* If NO_HEADER_TEXT is false, you would style the text with these selectors:
	#headimg #name { }
	#headimg #desc { }
*/
</style>
<?php
}
endif;

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * To override this in a child theme, remove the filter and optionally add
 * your own function tied to the wp_page_menu_args filter hook.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'twentyten_page_menu_args' );

/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Twenty Ten 1.0
 * @return int
 */
function twentyten_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'twentyten_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @since Twenty Ten 1.0
 * @return string "Continue Reading" link
 */
function twentyten_continue_reading_link() {
	return ' <br /><a class="read_more" href="'. get_permalink() . '">' . __( 'READ MORE <span class="meta-nav">&gt;</span>', 'twentyten' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyten_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string An ellipsis
 */
function twentyten_auto_excerpt_more( $more ) {
	return ' &hellip;' . twentyten_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function twentyten_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= twentyten_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'twentyten_custom_excerpt_more' );

/**
 * Remove inline styles printed when the gallery shortcode is used.
 *
 * Galleries are styled by the theme in Twenty Ten's style.css. This is just
 * a simple filter call that tells WordPress to not use the default styles.
 *
 * @since Vitrue 3.0
 */
add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * Deprecated way to remove inline styles printed when the gallery shortcode is used.
 *
 * This function is no longer needed or used. Use the use_default_gallery_style
 * filter instead, as seen above.
 *
 * @since Twenty Ten 1.0
 * @deprecated Deprecated in Twenty Ten 1.2 for WordPress 3.1
 *
 * @return string The gallery style filter, with the styles themselves removed.
 */
function twentyten_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
// Backwards compatibility with WordPress 3.0.
if ( version_compare( $GLOBALS['wp_version'], '3.1', '<' ) )
	add_filter( 'gallery_style', 'twentyten_remove_gallery_css' );

if ( ! function_exists( 'twentyten_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentyten_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'twentyten' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentyten' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'twentyten' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'twentyten' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyten' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'twentyten' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'twentyten' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'twentyten' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'twentyten' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'twentyten' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 6, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fourth Footer Widget Area', 'twentyten' ),
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'The fourth footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'twentyten_widgets_init' );

/**
 * Removes the default styles that are packaged with the Recent Comments widget.
 *
 * To override this in a child theme, remove the filter and optionally add your own
 * function tied to the widgets_init action hook.
 *
 * This function uses a filter (show_recent_comments_widget_style) new in WordPress 3.1
 * to remove the default style. Using Twenty Ten 1.2 in WordPress 3.0 will show the styles,
 * but they won't have any effect on the widget in default Twenty Ten styling.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_remove_recent_comments_style() {
	add_filter( 'show_recent_comments_widget_style', '__return_false' );
}
add_action( 'widgets_init', 'twentyten_remove_recent_comments_style' );

if ( ! function_exists( 'twentyten_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_posted_on() {
	printf( __( '<span class="%1$s">By</span> %3$s - %2$s  ', 'twentyten' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;

if ( ! function_exists( 'twentyten_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function twentyten_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;

/*
Adding Metaboxes to Pages for tracking codes
*/

add_action( 'add_meta_boxes', 'add_tracking' );
add_action( 'save_post', 'save_tracking' );

function add_tracking(){
	add_meta_box("tracking-meta", "Tracking tags", "meta_options", "page", "normal", "low");
}

function meta_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$google = $custom["google"][0];
	$omniture = $custom["omniture"][0];
	$manticore = $custom["manticore"][0];
	$dfa = $custom["dfa"][0];
	$x1 = $custom["x1"][0];
	
?>
<label style="display:block;font-weight:bold;margin-bottom:5px;">Omniture Analytics:</label>
<textarea name="omniture" style="margin-bottom:5px;width:800px;height:200px;overflow:auto;"><?php echo $omniture; ?></textarea>
<label style="display:block;font-weight:bold;margin-bottom:5px;">Google Analytics:</label>
<textarea name="google" style="margin-bottom:5px;width:800px;height:200px;overflow:auto;"><?php echo $google; ?></textarea>
<label style="display:block;font-weight:bold;margin-bottom:5px;">Manticore Analytics:</label>
<textarea name="manticore" style="margin-bottom:5px;width:800px;height:200px;overflow:auto;"><?php echo $manticore; ?></textarea>
<label style="display:block;font-weight:bold;margin-bottom:5px;">DFA Analytics:</label>
<textarea name="dfa" style="margin-bottom:5px;width:800px;height:200px;overflow:auto;"><?php echo $dfa; ?></textarea>
<label style="display:block;font-weight:bold;margin-bottom:5px;">X+1 Analytics:</label>
<textarea name="x1" style="margin-bottom:5px;width:800px;height:200px;overflow:auto;"><?php echo $x1; ?></textarea>
<?php }

function save_tracking(){
global $post;

update_post_meta($post->ID, "google", $_POST["google"]);
update_post_meta($post->ID, "omniture", $_POST["omniture"]);
update_post_meta($post->ID, "manticore", $_POST["manticore"]);
update_post_meta($post->ID, "dfa", $_POST["dfa"]);
update_post_meta($post->ID, "x1", $_POST["x1"]);

}

if (function_exists('register_sidebar')) {
	register_sidebar(array('name'=>'sidebar-product'));
	register_sidebar(array('name'=>'sidebar-landing'));
	register_sidebar(array('name'=>'sidebar-tv'));
	register_sidebar(array('name'=>'sidebar-pr'));
	register_sidebar(array('name'=>'sidebar-blog'));
} 


add_action( 'add_meta_boxes', 'add_hero' );
add_action( 'save_post', 'save_hero' );

function add_hero(){
	add_meta_box("hero-meta", "Hero Image", "hero_options", "page", "normal", "low");
}

function hero_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$large_image = $custom["large_image"][0];
	$large_url = $custom["large_url"][0];
	$button_url = $custom["button_url"][0];
	$small_image = $custom["small_image"][0];
		
?>
<label style="display:block;font-weight:bold;margin-bottom:5px;">Large Image URL:</label>
<input type="text" name="large_image" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $large_image; ?>" />

<label style="display:block;font-weight:bold;margin-bottom:5px;">URL for CTA button image:</label>
<input type="text" name="button_url" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $button_url; ?>" />

<label style="display:block;font-weight:bold;margin-bottom:5px;">URL for CTA click:</label>
<input type="text" name="large_url" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $large_url; ?>" />

<hr />

<label style="display:block;font-weight:bold;margin-bottom:5px;">Small Image URL:</label>
<input type="text" name="small_image" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $small_image; ?>" />
<?php }

function save_hero(){
global $post;

update_post_meta($post->ID, "large_image", $_POST["large_image"]);
update_post_meta($post->ID, "large_url", $_POST["large_url"]);
update_post_meta($post->ID, "button_url", $_POST["button_url"]);
update_post_meta($post->ID, "color_scheme", $_POST["color_scheme"]);
update_post_meta($post->ID, "small_image", $_POST["small_image"]);

}

add_action( 'add_meta_boxes', 'add_custom_meta' );
add_action( 'save_post', 'save_custom_stuff' );

function add_custom_meta(){
	add_meta_box("custom-meta", "Add Date and Location", "custom_stuff", "event-calendar", "normal", "low");
}

function custom_stuff(){
	global $post;
	$custom = get_post_custom($post->ID);
	$event_date = $custom["event_date"][0];
	$event_location = $custom["event_location"][0]; ?>
	
<label style="display:block;font-weight:bold;margin-bottom:5px;">Event Date:</label>
<input type="text" name="event_date" id="event_date" style="margin-bottom:5px;width:200px;padding:5px;" value="<?php echo $event_date; ?>" />

<label style="display:block;font-weight:bold;margin-bottom:5px;">Location (City, State):</label>
<input type="text" name="event_location" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $event_location; ?>" />
<?php }

function save_custom_stuff(){
global $post;

update_post_meta($post->ID, "event_date", $_POST["event_date"]);
update_post_meta($post->ID, "event_location", $_POST["event_location"]);
}

/*

Breadcrumb function

*/

function dimox_breadcrumbs() {
 
  $delimiter = '&gt;';
  $home = 'Home'; // text for the 'Home' link
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
    echo '<div id="crumbs">';
 
    global $post;
    $homeLink = get_bloginfo('url');
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} // end dimox_breadcrumbs()

function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}

add_action( 'init', 'set_custom_menus' );

function set_custom_menus() {
	
	register_nav_menus(array('hdr-menu' => 'Header Menu', 'footer-menu' => 'Footer Menu'));

}

add_action( 'init', 'create_post_type' );

function create_post_type() {
	register_post_type( 'event-calendar',
		array(
			'labels' => array(
				'name' => __( 'Events' ),
				'singular_name' => __( 'Event' ),
				'add_new_item' => 'Add New Event'				
			),
		'rewrite' => true,
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'hierarchical' => false
		)
	);
}

add_action( 'init', 'create_custom_one' );

function create_custom_one() {
	register_post_type( 'custom-one',
		array(
			'labels' => array(
				'name' => __( 'Front Custom Top' ),
				'singular_name' => __( 'Front_One' ),
				'add_new_item' => 'Add New Event'				
			),
		'rewrite' => true,
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'hierarchical' => false,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'capabilities' => array(
			'publish_posts' => 'edit_pages',
			'edit_posts' => 'edit_pages',
			'edit_others_posts' => 'edit_pages',
			'delete_posts' => 'edit_pages',
			'delete_others_posts' => 'edit_pages',
			'read_private_posts' => 'edit_pages',
			'edit_post' => 'edit_pages',
			'delete_post' => 'edit_pages',
			'read_post' => 'edit_pages',
			'manage_categories' => 'edit_pages'
		),
		)
	);
}

add_action( 'init', 'create_custom_two' );

function create_custom_two() {
	register_post_type( 'custom-two',
		array(
			'labels' => array(
				'name' => __( 'Front Custom Bottom' ),
				'singular_name' => __( 'Front_Two' ),
				'add_new_item' => 'Add New Event'				
			),
		'rewrite' => true,
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'hierarchical' => false,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'capabilities' => array(
			'publish_posts' => 'edit_pages',
			'edit_posts' => 'edit_pages',
			'edit_others_posts' => 'edit_pages',
			'delete_posts' => 'edit_pages',
			'delete_others_posts' => 'edit_pages',
			'read_private_posts' => 'edit_pages',
			'edit_post' => 'edit_pages',
			'delete_post' => 'edit_pages',
			'read_post' => 'edit_pages',
			'manage_categories' => 'edit_pages'
		),
		)
	);
}

add_action( 'add_meta_boxes', 'add_custom_url_one' );
add_action( 'save_post', 'save_custom_url_one' );

function add_custom_url_one(){
	add_meta_box("custom-meta", "Input URL to Read More", "custom_stuff_one", "custom-one", "normal", "low");
}

function custom_stuff_one(){
	global $post;
	$custom = get_post_custom($post->ID);
	$custom_url_one = $custom["custom_url_one"][0]; ?>
	
<label style="display:block;font-weight:bold;margin-bottom:5px;">URL to read more:</label>
<input type="text" name="custom_url_one" id="custom_url_one" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $custom_url_one; ?>" />
<?php }

function save_custom_url_one(){
global $post;

update_post_meta($post->ID, "custom_url_one", $_POST["custom_url_one"]);
}

add_action( 'add_meta_boxes', 'add_custom_url_two' );
add_action( 'save_post', 'save_custom_url_two' );

function add_custom_url_two(){
	add_meta_box("custom-meta", "Input URL to Read More", "custom_stuff_two", "custom-two", "normal", "low");
}

function custom_stuff_two(){
	global $post;
	$custom = get_post_custom($post->ID);
	$custom_url_two = $custom["custom_url_two"][0]; ?>
	
<label style="display:block;font-weight:bold;margin-bottom:5px;">URL to read more:</label>
<input type="text" name="custom_url_two" id="custom_url_two" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $custom_url_two; ?>" />
<?php }

function save_custom_url_two(){
global $post;

update_post_meta($post->ID, "custom_url_two", $_POST["custom_url_two"]);
}

add_action( 'init', 'job_listings' );

function job_listings() {
	register_post_type( 'job_listing',
		array(
			'labels' => array(
				'name' => __( 'Job Listings' ),
				'singular_name' => __( 'job_listing' ),
				'add_new_item' => 'Add New Job'				
			),
		'rewrite' => true,
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'hierarchical' => true,
		'supports' => array( 'title', 'editor' ),
		'taxonomies' => array( 'category', 'page-category' ),
		'capability' => 'jobs',
		'capabilities' => array(
			'publish_posts' => 'add_jobs',
			'edit_posts' => 'add_jobs',
			'edit_others_posts' => 'add_jobs',
			'delete_posts' => 'add_jobs',
			'delete_others_posts' => 'add_jobs',
			'read_private_posts' => 'add_jobs',
			'edit_post' => 'add_jobs',
			'delete_post' => 'add_jobs',
			'read_post' => 'add_jobs',
			'manage_categories' => 'manage_jobs')
		)
	);
}

add_action( 'add_meta_boxes', 'add_job_listing' );
add_action( 'save_post', 'save_job_listing' );

function add_job_listing(){
	add_meta_box("jobs-meta", "Location of position (City, State)", "write_job", "job_listing", "normal", "low");
}

function write_job(){
	global $post;
	$custom = get_post_custom($post->ID);
	$job_location = $custom["job_location"][0]; ?>
	
<label style="display:block;font-weight:bold;margin-bottom:5px;">Job location:</label>
<input type="text" name="job_location" id="job_location" style="margin-bottom:5px;width:300px;padding:5px;" value="<?php echo $job_location; ?>" />
<small style="display:block">Feel free to add multiple locations in this box</small>
<?php }

function save_job_listing(){
global $post;

update_post_meta($post->ID, "job_location", $_POST["job_location"]);
}

add_action( 'init', 'product_posts' );

function product_posts() {
	register_post_type( 'product-blog',
		array(
			'labels' => array(
				'name' => __( 'Product Blog' ),
				'singular_name' => __( 'product-blog' ),
				'add_new_item' => 'Add New Post'				
			),
		'rewrite' => true,
		'public' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('product'),
		'capability' => 'product_blog',
		'capabilities' => array(
			'publish_posts' => 'add_product_blog',
			'edit_posts' => 'add_product_blog',
			'edit_others_posts' => 'add_product_blog',
			'delete_posts' => 'add_product_blog',
			'delete_others_posts' => 'add_product_blog',
			'read_private_posts' => 'add_product_blog',
			'edit_post' => 'add_product_blog',
			'delete_post' => 'add_product_blog',
			'read_post' => 'add_product_blog',
			'manage_categories' => 'add_product_blog' )
		)
	);
}

add_action( 'init', 'build_taxonomies', 0 );  
  
function build_taxonomies() {  
    register_taxonomy( 'product', 'product-blog', 
    	array( 
	    	'hierarchical' => true, 
	    	'label' => 'Categories', 
	    	'query_var' => true, 
	    	'rewrite' => true,
	    	'capabilities' => array(
	    		'manage_terms' => 'add_product_blog',
            	'edit_terms' => 'add_product_blog',
            	'delete_terms' => 'add_product_blog',
		    	'assign_terms' => 'add_product_blog'
		    ) 
	    ) 
	);
	
	// register_taxonomy( 'department', 'job_listing', 
 //    	array( 
	//     	'hierarchical' => true, 
	//     	'label' => 'Departments', 
	//     	'query_var' => true, 
	//     	'rewrite' => true,
	//     	'capabilities' => array(
	//     		'manage_terms' => 'add_jobs',
 //            	'edit_terms' => 'add_jobs',
 //            	'delete_terms' => 'add_jobs',
	// 	    	'assign_terms' => 'add_jobs'
	// 	    ) 
	//     ) 
	// );   
} 

// $result = add_role('hr', 'HR', array(
// 	'read' => true, 
// 	'edit_posts' => true,
// 	'add_jobs' => true,
// 	'edit_jobs' => true,
// 	'delete_jobs' => true,
//  	'manage_department' => true,
//  	'manage_jobs' => true,
//  	'manage_categories' => true
// ));
// if (null !== $result) {
//     echo 'Yay!  New role created!';
// } else {
//     echo 'Oh... that role already exists.';
// }

 // $result = add_role('product-blog', 'Product Blog', array(
 // 	'read' => true, 
 // 	'add_product_blog' => true,
 // 	'edit_product_blog' => true,
 // 		'delete_product_blog' => true,
 // 	'manage_product' => true,
 // 	'manage_product_blog' => true,
 // 	'upload_files' => true,
 // 	'manage_files' => true,
 // 	'edit_files' => true
 // ));
 // if (null !== $result) {
 //     echo 'Yay!  New role created!';
 // } else {
 //     echo 'Oh... that role already exists.';
 // }

// remove_role('hr');
// remove_role('product-blog');

function posts_for_current_author($query) {
        global $pagenow;
    if( 'edit.php' != $pagenow || !$query->is_admin )
        return $query;
    if( !current_user_can( 'manage_options' ) ) {
       global $user_ID;
       $query->set('hr', $user_ID );
     }
     return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');
 
/* START: Omit Search Filter */
 
function SearchFilter($query){
  if($query->is_search){
 
    // List of categories (by ID) to omit when searching. Category ID should be placed in the array.
// 3 - Portfolio
    // $categoryOmitArray = array('3');
 
    // List of pages/posts to omit when searching.  Page/post ID should be placed in the array.
// 7  - 404 - Not Found (page)
// 25 - Resume (protected page)
    $postOmitArray = array('9945', '9731');
 
    // Set the pages/posts and categories to exclude in the WP Query
    $query->set('category__not_in', $categoryOmitArray);
    $query->set('post__not_in', $postOmitArray);
  }
  return $query;
}
add_filter('pre_get_posts','SearchFilter');
/* END: Omit Search Filter */
 
?>