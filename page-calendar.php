<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Calendar
 */
 
 get_template_part( 'loop', 'page' );
?>
             
<script type="text/javascript">
$('a#next').click(function(event){
   /* console.log("Clicked Me");*/
	event.preventDefault();
	var page = $(this).attr('href');
	/*alert(page);*/
	$('#modal #modal_cont').load(page);
});

$('a#prev').click(function(event){
   /* console.log("Clicked Me");*/
	event.preventDefault();
	var page = $(this).attr('href');
	/*alert(page);*/
	$('#modal #modal_cont').load(page);
});
</script>			