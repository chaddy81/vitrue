<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Vitrue TV
 */

get_header();

global $post;
$custom = get_post_custom($post->ID);
$large_image = $custom["large_image"][0];
$large_url = $custom["large_url"][0];
$button_text = $custom["large_url"][0];
$color_scheme = $custom["color_scheme"][0];
$small_image = $custom["small_image"][0]; ?>
	
<section id="slider_nh"></section>
<section id="content_mid" class="nh">
<div id="container_mid">
	<div id="content" role="main">
    	<section id="main_content">
          <?php
			get_template_part( 'loop', 'page' );
		  ?>
          <div id="videos2"></div>
          <div class="clear"></div>
          
		</section>
        <?php get_sidebar('tv'); ?>
		<div class="clear"></div>
	</div><!-- #content -->
    <span id="container_end"></span>
</div><!-- #container -->
<script type="text/javascript">
 
function showMyVideos2(data) {
  var feed = data.feed;
  var entries = feed.entry || [];
  var html = ['<div id="videos">'];
  for (var i = 0; i < entries.length; i++) {
    var entry = entries[i];
    var title = entry.title.$t;
	var cat = entries[i].media$group.media$category[0].label;
	var date = new Date(entry.published.$t);
	var month = date.getMonth();
	var day = date.getDate();
	var year = date.getFullYear();
	var upDate = month + '/' + day + '/' + year;
	var desc = entries[i].media$group.media$description.$t.substr(0,75);
	var desc_long = entries[i].media$group.media$description.$t;
    var thumbnailUrl = entries[i].media$group.media$thumbnail[0].url;
    var playerUrl = entries[i].media$group.media$content[0].url;

    html.push('<div class="vid_cont" data-category="',cat,'" data-date="', upDate, '"><img src="<?php bloginfo('template_directory'); ?>/scripts/timthumb.php?src=', thumbnailUrl, '&h=98&w=175&zc=1" class="yt_thumb"/><a href="', playerUrl, '" target="_blank" class="open_vid" title="', desc_long, '">',
              '<span class="titlec">', title, '</span></a><a href="', playerUrl,'" title="', desc_long, '" target="_blank" class="demo open_vid">Play Video<img src="<?php bloginfo('template_directory'); ?>/images/white_arrow.png" /></a></div>');
  }
  html.push('<div class="clear"></div>');
  document.getElementById('videos2').innerHTML = html.join('');
  if (entries.length > 0) {
    loadVideo(entries[0].media$group.media$content[0].url, false);
  }
  
}
</script>
<script type="text/javascript" src="http://gdata.youtube.com/feeds/users/vitrue/uploads?alt=json-in-script&callback=showMyVideos2"></script>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>