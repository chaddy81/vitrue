<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */


get_header(); ?>

	<div id="slider_nh"></div>
      <section id="content_mid" class="nh">
		<div id="container_mid"> 
			<div id="content" role="main">
				<section id="main_content">

				<h1 class="page-title">Product Blog Archive</h1>
				<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts( "product=product-admin&paged=$paged" );

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'product-blog' );
				?>
		</section>
                <?php get_sidebar('product-blog'); ?>
				<div class="clear"></div>
			</div><!-- #content -->
            <span id="container_end"></span>
		</div><!-- #container -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>