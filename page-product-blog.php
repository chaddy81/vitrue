<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Product Blog
 */

get_header(); 
global $post;
$custom = get_post_custom($post->ID);
$large_image = $custom["large_image"][0];
$large_url = $custom["large_url"][0];
$button_text = $custom["large_url"][0];
$color_scheme = $custom["color_scheme"][0];
$small_image = $custom["small_image"][0];      
?>

	  <section id="slider_nh">	
      	<img src="<?php echo $small_image; ?>" style="margin-left:25px;" />
      </section>
      <section id="content_mid" class="blog">
		<div id="container_mid">
        
			<div id="content" role="main">
            	<section id="main_content">
                	<?php
					  $args = array( 'post_type' => 'product-blog' );
					  query_posts( $args );
					 
					  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
							<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>	
							<div class="entry-meta">
								<?php twentyten_posted_on(); ?>
							</div><!-- .entry-meta -->
							<div class="entry-content">
								<?php the_content( __( 'READ MORE', 'twentyten' ) ); ?>
								<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							</div><!-- .entry-content -->	 
							<div class="clear"></div>
							<div class="entry-utility">
								<?php if ( count( get_the_category() ) ) : ?>
									<span class="cat-links">
										<?php printf( __( '<span class="%1$s">Posted in</span> %2$s', 'twentyten' ), 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list( ', ' ) ); ?>
									</span>
									<span class="meta-sep">|</span>
								<?php endif; ?>
								<?php
									$tags_list = get_the_tag_list( '', ', ' );
									if ( $tags_list ):
								?>
									<span class="tag-links">
										<?php printf( __( '<span class="%1$s">Tagged</span> %2$s', 'twentyten' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list ); ?>
									</span>
									<span class="meta-sep">|</span>
								<?php endif; ?>
								<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyten' ), __( '1 Comment', 'twentyten' ), __( '% Comments', 'twentyten' ) ); ?></span>
								<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
							</div><!-- .entry-utility --> 
					<?php
					  endwhile;
					  endif;
				  	?>
                  
				</section>
                <?php get_sidebar('product-blog'); ?>
				<div class="clear"></div>
			</div><!-- #content -->
            <span id="container_end"></span>
		</div><!-- #container -->
        

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
