<?php
/**
 * The Sidebar for Accelerator landing page.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */
?>

<aside id="sidebar">
    <a href="http://vitrue.com/request-a-demo"><span style="color: #676768;background: none;border: none;box-shadow: none;-moz-box-shadow: none;-webkit-box-shadow: none;display: block;font-weight: 500;text-transform: uppercase;padding-left:15px;">SIGN UP FOR A<br /><span style="color: #98B226;font-size: 25px;font-weight: 700;line-height: 26px;padding: 0 5px 25px 0;">LIVE DEMO<img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></span></a>
    
    <img src="http://www.vitrue.com/wp-content/uploads/2011/09/acc_cost.png" style="margin-top:20px;margin-bottom:5px;" />
    <a href="http://www.vitrue.com/accelerator/purchase" class="demo" style="padding-top:25px;padding-bottom:25px;"><span>ORDER NOW<img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></span></a>
</aside>