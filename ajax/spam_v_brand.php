<img src="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/images/wp_modal_icon.png" style="float:left;margin-right:10px;vertical-align:-5px;display:block;margin-top:5px;" />
<h1 style="margin-top:0 !important;margin-bottom:10px !important;">The Guests that Never Leave</h1>
<p style="font-family: 'Cabin',sans-serif;font-size:15px;text-transform:none;margin-bottom:30px;margin-top:-5px;color:#3b4b79;text-shadow:none;font-weight:bold;">
      Learn How to Protect Your Social Community from Unwanted Content
</p>

<div class="clear"></div>
<div style="width:285px;padding:0 20px;height:445px;position:absolute;left:0;bottom:0;background:#3b4b79;">
  <p style="color:#fff;font:bold 35px/35px 'Ubuntu', arial, sans-serif;text-transform:uppercase;margin-top:20px;margin-bottom:40px;">Learn How To:</p>
  <ul style="margin-left:20px;">
    <li style="font-size:18px;font-weight:200;line-height:24px;color:#fff;list-style-type:disc;margin-bottom:50px;">Establish rules of engagement with your community</li>
    <li style="font-size:18px;font-weight:200;line-height:24px;color:#fff;list-style-type:disc;margin-bottom:50px;">Use filters and tools to moderate user-generated content</li>
    <li style="font-size:18px;font-weight:200;line-height:24px;color:#fff;list-style-type:disc;margin-bottom:50px;">Effectively communicate with your community</li>
  </ul>
</div>
<form id="mant_form" style="float:right">
  <div>
    <fieldset>
      <label>First Name: </label>
      <input type="text" name="first" placeholder="First Name" style="margin-bottom:14px;" />
    </fieldset>
    <fieldset>
      <label>Last Name: </label>
      <input type="text" name="last" placeholder="Last Name" style="margin-bottom:14px;" />
    </fieldset>
    <fieldset>
      <label>Company: </label>
      <input type="text" name="company" placeholder="Company" style="margin-bottom:14px;" />
    </fieldset>
  </div>
  <div>
    <fieldset>
      <label>Email: </label>
      <input type="text" name="email" placeholder="Email" style="margin-bottom:14px;" />
    </fieldset>
    <fieldset>
      <label>Phone: </label>
      <input type="text" name="phone" placeholder="Phone" style="margin-bottom:14px;" />
    </fieldset>
    <fieldset>
      <label>Country</label>
      <select name="country">
        <option>United States</option>
        <option>Afghanistan</option>
        <option>Albania</option>
        <option>Algeria</option>
        <option>American Samoa</option>
        <option>Andorra</option>
        <option>Angola</option>
        <option>Anguilla</option>
        <option>Antarctica</option>
        <option>Antigua and Barbuda</option>
        <option>Argentina</option>
        <option>Armenia</option>
        <option>Aruba</option>
        <option>Australia</option>
        <option>Austria</option>
        <option>Azerbaijan</option>
        <option>Bahamas</option>
        <option>Bahrain</option>
        <option>Bangladesh</option>
        <option>Barbados</option>
        <option>Belarus</option>
        <option>Belgium</option>
        <option>Belize</option>
        <option>Benin</option>
        <option>Bermuda</option>
        <option>Bhutan</option>
        <option>Bolivia</option>
        <option>Botswana</option>
        <option>Bouvet Island</option>
        <option>Brazil</option>
        <option>Bulgaria</option>
        <option>Burundi</option>
        <option>Cambodia</option>
        <option>Cameroon</option>
        <option>Canada</option>
        <option>Cape Verde</option>
        <option>Cayman Islands</option>
        <option>Central African Republic</option>
        <option>Chad</option>
        <option>Chile</option>
        <option>China</option>
        <option>Christmas Island</option>
        <option>Cocos Islands</option>
        <option>Colombia</option>
        <option>Comoros</option>
        <option>Congo</option>
        <option>Cook Islands</option>
        <option>Costa Rica</option>
        <option>Cote d'Ivoire</option>
        <option>Croatia</option>
        <option>Cuba</option>
        <option>Cyprus</option>
        <option>Czech Republic</option>
        <option>Denmark</option>
        <option>Djibouti</option>
        <option>Dominica</option>
        <option>Dominican Republic</option>
        <option>Ecuador</option>
        <option>Egypt</option>
        <option>El Salvador</option>
        <option>Equatorial Guinea</option>
        <option>Eritrea</option>
        <option>Estonia</option>
        <option>Ethiopia</option>
        <option>Falkland Islands</option>
        <option>Faroe Islands</option>
        <option>Fiji</option>
        <option>Finland</option>
        <option>France</option>
        <option>French Guiana</option>
        <option>French Polynesia</option>
        <option>Gabon</option>
        <option>Gambia</option>
        <option>Georgia</option>
        <option>Germany</option>
        <option>Ghana</option>
        <option>Gibraltar</option>
        <option>Greece</option>
        <option>Greenland</option>
        <option>Grenada</option>
        <option>Guadeloupe</option>
        <option>Guam</option>
        <option>Guatemala</option>
        <option>Guinea</option>
        <option>Guinea-Bissau</option>
        <option>Guyana</option>
        <option>Haiti</option>
        <option>Honduras</option>
        <option>Hong Kong</option>
        <option>Hungary</option>
        <option>Iceland</option>
        <option>India</option>
        <option>Indonesia</option>
        <option>Iran</option>
        <option>Iraq</option>
        <option>Ireland</option>
        <option>Israel</option>
        <option>Italy</option>
        <option>Jamaica</option>
        <option>Japan</option>
        <option>Jordan</option>
        <option>Kazakhstan</option>
        <option>Kenya</option>
        <option>Kiribati</option>
        <option>Kuwait</option>
        <option>Kyrgyzstan</option>
        <option>Laos</option>
        <option>Latvia</option>
        <option>Lebanon</option>
        <option>Lesotho</option>
        <option>Liberia</option>
        <option>Libya</option>
        <option>Liechtenstein</option>
        <option>Lithuania</option>
        <option>Luxembourg</option>
        <option>Macao</option>
        <option>Madagascar</option>
        <option>Malawi</option>
        <option>Malaysia</option>
        <option>Maldives</option>
        <option>Mali</option>
        <option>Malta</option>
        <option>Marshall Islands</option>
        <option>Martinique</option>
        <option>Mauritania</option>
        <option>Mauritius</option>
        <option>Mayotte</option>
        <option>Mexico</option>
        <option>Micronesia</option>
        <option>Moldova</option>
        <option>Monaco</option>
        <option>Mongolia</option>
        <option>Montenegro</option>
        <option>Montserrat</option>
        <option>Morocco</option>
        <option>Mozambique</option>
        <option>Myanmar</option>
        <option>Namibia</option>
        <option>Nauru</option>
        <option>Nepal</option>
        <option>Netherlands</option>
        <option>Netherlands Antilles</option>
        <option>New Caledonia</option>
        <option>New Zealand</option>
        <option>Nicaragua</option>
        <option>Niger</option>
        <option>Nigeria</option>
        <option>Norfolk Island</option>
        <option>North Korea</option>
        <option>Norway</option>
        <option>Oman</option>
        <option>Pakistan</option>
        <option>Palau</option>
        <option>Palestinian Territory</option>
        <option>Panama</option>
        <option>Papua New Guinea</option>
        <option>Paraguay</option>
        <option>Peru</option>
        <option>Philippines</option>
        <option>Pitcairn</option>
        <option>Poland</option>
        <option>Portugal</option>
        <option>Puerto Rico</option>
        <option>Qatar</option>
        <option>Romania</option>
        <option>Russian Federation</option>
        <option>Rwanda</option>
        <option>Saint Helena</option>
        <option>Saint Kitts and Nevis</option>
        <option>Saint Lucia</option>
        <option>Saint Pierre and Miquelon</option>
        <option>Saint Vincent and the Grenadines</option>
        <option>Samoa</option>
        <option>San Marino</option>
        <option>Sao Tome and Principe</option>
        <option>Saudi Arabia</option>
        <option>Senegal</option>
        <option>Serbia</option>
        <option>Seychelles</option>
        <option>Sierra Leone</option>
        <option>Singapore</option>
        <option>Slovakia</option>
        <option>Slovenia</option>
        <option>Solomon Islands</option>
        <option>Somalia</option>
        <option>South Africa</option>
        <option>South Georgia</option>
        <option>South Korea</option>
        <option>Spain</option>
        <option>Sri Lanka</option>
        <option>Sudan</option>
        <option>Suriname</option>
        <option>Svalbard and Jan Mayen</option>
        <option>Swaziland</option>
        <option>Sweden</option>
        <option>Switzerland</option>
        <option>Syrian Arab Republic</option>
        <option>Taiwan</option>
        <option>Tajikistan</option>
        <option>Tanzania</option>
        <option>Thailand</option>
        <option>Timor-Leste</option>
        <option>Togo</option>
        <option>Tokelau</option>
        <option>Tonga</option>
        <option>Trinidad and Tobago</option>
        <option>Tunisia</option>
        <option>Turkey</option>
        <option>Turkmenistan</option>
        <option>Tuvalu</option>
        <option>Uganda</option>
        <option>Ukraine</option>
        <option>United Arab Emirates</option>
        <option>United Kingdom</option>
        <option>United States Minor Outlying Islands</option>
        <option>Uruguay</option>
        <option>Uzbekistan</option>
        <option>Vanuatu</option>
        <option>Vatican City</option>
        <option>Venezuela</option>
        <option>Vietnam</option>
        <option>Virgin Islands, British</option>
        <option>Virgin Islands, U.S.</option>
        <option>Wallis and Futuna</option>
        <option>Western Sahara</option>
        <option>Yemen</option>
        <option>Zambia</option>
        <option>Zimbabwe</option>
      </select>
    </fieldset>
  </div>
  <div>
    <fieldset>
      <label>State</label>
      <select name="state">
        <option>Alabama</option>
        <option>Alaska</option>
        <option>Arizona</option>
        <option>Arkansas</option>
        <option>California</option>
        <option>Colorado</option>
        <option>Connecticut</option>
        <option>Delaware</option>
        <option>Florida</option>
        <option>Georgia</option>
        <option>Hawaii</option>
        <option>Idaho</option>
        <option>Illinois</option>
        <option>Indiana</option>
        <option>Iowa</option>
        <option>Kansas</option>
        <option>Kentucky</option>
        <option>Louisiana</option>
        <option>Maine</option>
        <option>Maryland</option>
        <option>Massachusetts</option>
        <option>Michigan</option>
        <option>Minnesota</option>
        <option>Mississippi</option>
        <option>Missouri</option>
        <option>Montana</option>
        <option>Nebraska</option>
        <option>Nevada</option>
        <option>New Hampshire</option>
        <option>New Jersey</option>
        <option>New Mexico</option>
        <option>New York</option>
        <option>North Carolina</option>
        <option>North Dakota</option>
        <option>Ohio</option>
        <option>Oklahoma</option>
        <option>Oregon</option>
        <option>Pennsylvania</option>
        <option>Rhode Island</option>
        <option>South Carolina</option>
        <option>South Dakota</option>
        <option>Tennessee</option>
        <option>Texas</option>
        <option>Utah</option>
        <option>Vermont</option>
        <option>Virginia</option>
        <option>Washington</option>
        <option>West Virginia</option>
        <option>Wisconsin</option>
        <option>Wyoming</option>
      </select>
    </fieldset>
    <fieldset>
      <label style="margin-bottom:10px;width:175px;">Are you interested in a free trial?
        <input type="checkbox" name="trial" id="Free_trial__c_0" value="true" />
      </label>
    </fieldset>
  </div>
  <div>
    <fieldset>
      <label>What is your Facebook fan count?</label>
      <select name="fan_count">
        <option value="100,000">100,000+</option>
        <option value="50,000-100,000">50,000-100,000</option>
        <option value="20,000-50,000">20,000-50,000</option>
        <option value="5,000-20,000">5,000-20,000</option>
        <option value="0-5,000">0-5,000</option>
      </select>
    </fieldset>
    <fieldset>
      <label style="width:225px;">How soon do you need to implement a Social Relationship Management platform?</label>
      <select name="implement">
        <option>ASAP</option>
        <option>1-3 months</option>
        <option>3-6 months</option>
        <option>1 year +</option>
        <option>Already have a platform in place</option>
      </select>
    </fieldset>
  </div>
    <input type="submit" name="submit" id="mant_submit" />
</form>


<script type="text/javascript">
(function() {
  var wp = $('#modal_cont').attr('data-paper');
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
  $('input#mant_submit').click(function(event) {
	  event.preventDefault();
	  if($('input[name="first"]').val().length == 0 || $('input[name="last"]').val().length == 0 || $('input[name="company"]').val().length == 0 || $('input[name="email"]').val().length == 0 || $('input[name="phone"]').val().length == 0){
		  alert("Please Fill Out Entire Form");
	  }else{
	
		  return $.ajax({
			type: "GET",
			url: 'http://www.vitrue.com/wp-content/themes/Vitrue-3.0/process.php',
			data: $('#mant_form').serialize(),
			success: __bind(function(){
				_gaq.push(['_trackEvent', 'WhitePapers', 'Download - Final', wp]);
				$('#modal #modal_cont').html('<h4 style="margin-top:50px;">Thank you for downloading our white paper! Your download should start automatically or click <a href="http://www.vitrue.com/wp-content/themes/Vitrue-3.0/white-papers/' + wp + '.pdf" target="_blank">here</a> to download your white paper manually.</h4>');
				window.open("http://www.vitrue.com/wp-content/themes/Vitrue-3.0/white-papers/" + wp + ".pdf","_blank");
				fireTags();
			}, this)
		  });
	  }
  });
}).call(this);

</script>