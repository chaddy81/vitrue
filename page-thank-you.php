<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Thank You
 */


 $filepath = 'vitrue.com/wp-content/uploads/2011/07/';
 $filename = $_POST['wp_name'];
 $file = $filepath . $filename;

 //CREATE/OUTPUT THE HEADER
header('Content-Description: File Transfer');  
header('Content-Type: application/octet-stream');  
header('Content-Disposition: attachment; filename="'.$filename .'"');  
header('Content-Transfer-Encoding: binary');  
header('Expires: 0');  
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');  
header('Pragma: public');  
header('Content-Length: ' . filesize($file));



get_header(); 
        
?>

<section id="slider_nh"></section>
<section id="content_mid" class="product">
<div id="container_mid">
	<div id="content" role="main">
    	<section id="main_content">
          <?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
		  ?>
          <!--<div id="share">
          	<span>share: <a href="#" class="share_fb"></a><a href="#" class="share_tw"></a><a href="#" class="share_email"></a></span>
          </div>-->
		</section>
        <?php get_sidebar('product'); ?>
		<div class="clear"></div>
	</div><!-- #content -->
    <span id="container_end"></span>
</div><!-- #container -->
        

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
