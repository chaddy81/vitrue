<?php
/**
 *
 * @package WordPress
 * @subpackage 
 * @since Vitrue 3.0
 */
 
global $post;
$custom = get_post_custom($post->ID);
$job_location = $custom["job_location"][0];

?>

<h1>About Us: Job Listings</h1>
                   	<p> Our Current Openings are listed below, though note that new positions and opportunities arise unexpectedly. If you're interested in what we do, and are great at what you do, let us know how you think you can help us!</p>
                    
                    <br />

                    <h2 style="color:#ae006e;">Hot Jobs</h2>
                    <?php //echo $job_location; ?>
                    <?php
					$args = array( 'category_name' => 'hr-hot-jobs', 'post_type' => 'job_listing', 'numberposts' => -1, 'orderby' => 'post_date' );
					$lastposts = get_posts( $args );
					foreach($lastposts as $post) : setup_postdata($post); 
                    global $post;
					$custom = get_post_custom($post->ID);
					$job_location = $custom["job_location"][0];?>
						<strong><a href="<?php the_permalink(); ?>" style="text-decoration:none;margin-bottom:5px;"><?php the_title(); ?></a> (<span class="location"><?php echo $job_location; ?></span>)</strong><br />
					<?php endforeach; ?>

					<br />

                    <h2>Platform<br /><a href="mailto:platformjobs@vitrue.com" class="jobs-emails">platformjobs@vitrue.com</a></h2>
                    <?php //echo $job_location; ?>
                    <?php
					$args = array( 'category_name' => 'hr-development', 'post_type' => 'job_listing', 'numberposts' => -1, 'orderby' => 'post_date' );
					$lastposts = get_posts( $args );
					foreach($lastposts as $post) : setup_postdata($post); 
                    global $post;
					$custom = get_post_custom($post->ID);
					$job_location = $custom["job_location"][0];?>
						<a href="<?php the_permalink(); ?>" style="text-decoration:none;margin-bottom:5px;"><?php the_title(); ?></a> (<span class="location"><?php echo $job_location; ?></span>)<br />
					<?php endforeach; ?>

					<br />
                    
                    <h2>Client Services<br /><a href="mailto:csjobs@vitrue.com" class="jobs-emails">csjobs@vitrue.com</a></h2>
                    <?php
                    $args = array( 'category_name' => 'hr-client-services', 'post_type' => 'job_listing', 'numberposts' => -1, 'orderby' => 'post_date' );
					$lastposts = get_posts( $args );
					foreach($lastposts as $post) : setup_postdata($post); 
                    global $post;
					$custom = get_post_custom($post->ID);
					$job_location = $custom["job_location"][0];?>
						<a href="<?php the_permalink(); ?>" style="text-decoration:none;margin-bottom:5px;"><?php the_title(); ?></a> (<span class="location"><?php echo $job_location; ?></span>)<br />
					<?php endforeach; ?>

					<br />
                    
                    <h2>Sales<br /><a href="mailto:salesjobs@vitrue.com" class="jobs-emails">salesjobs@vitrue.com</a></h2>
                    <?php
					$args = array( 'category_name' => 'hr-sales', 'post_type' => 'job_listing', 'numberposts' => -1, 'orderby' => 'post_date' );
					$lastposts = get_posts( $args );
					foreach($lastposts as $post) : setup_postdata($post); 
					global $post;
					$custom = get_post_custom($post->ID);
					$job_location = $custom["job_location"][0];?>
						<a href="<?php the_permalink(); ?>" style="text-decoration:none;margin-bottom:5px;"><?php the_title(); ?></a> (<span class="location"><?php echo $job_location; ?></span>)<br />
					<?php endforeach; ?>

					<br />
                    
                    <h2>Marketing<br /><a href="mailto:mktjobs@vitrue.com" class="jobs-emails">mktjobs@vitrue.com</a></h2>
                    <?php
					$args = array( 'category_name' => 'hr-marketing', 'post_type' => 'job_listing', 'numberposts' => -1, 'orderby' => 'post_date' );
					$lastposts = get_posts( $args );
					foreach($lastposts as $post) : setup_postdata($post); 
					global $post;
					$custom = get_post_custom($post->ID);
					$job_location = $custom["job_location"][0];?>
						<a href="<?php the_permalink(); ?>" style="text-decoration:none;margin-bottom:5px;"><?php the_title(); ?></a> (<span class="location"><?php echo $job_location; ?></span>)<br />
					<?php endforeach; ?>
                    
					<br />

                    <p>For general employment inquiries: <a href="mailto:hr@vitrue.com" style="font-family: 'trebuchet ms';">hr@vitrue.com</a></p>
