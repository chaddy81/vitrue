<!-- SiteCatalyst code version: H.23.6.
Copyright 1996-2011 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/s_code.js"></script>
<script type="text/javascript">//<![CDATA[
/* You may give each page an identifying name, server, and channel on
the next lines. */

<?php
$post = $wp_query->get_queried_object();
$pagename = $post->post_name;
$get_parent = $post->post_parent;
$parent = str_replace(" ", "-", strtolower(get_the_title($get_parent)));
$get_grandparent = get_post_ancestors($get_parent);
$grandparent = str_replace(" ", "-", strtolower(get_the_title($get_grandparent[0])));

$url = $_SERVER['HTTP_HOST']; 
$script = $_SERVER['REQUEST_URI']; 
?>

s.pageName="<?php if(count($get_grandparent) !== 0){echo $grandparent . "::" . $parent . "::" . $pagename;}elseif($get_parent !== 0 && isset($get_parent)){ echo $parent . "::" . $pagename;}elseif($pagename !== '' && isset($pagename)){ echo $pagename; }else{ echo 'home';} ?>"
s.server=""
s.channel="<?php if(count($get_grandparent) !== 0){echo $grandparent;}elseif($get_parent !== 0 && isset($get_parent)){ echo $parent;}elseif($pagename !== '' && isset($pagename)){ echo $pagename; }else{ echo 'home';} ?>"
s.pageType=""
s.prop1="<?php if($get_parent !== 0 && isset($get_parent)){ echo $parent; } ?>"
s.prop2="<?php if(is_404()){ echo "http://".$url.$script; } ?>"
s.prop3=""
if (document.getElementsByClassName("not-found").length > 0){
	s.events=s.apl(s.events,"event2",",",2);
	s.prop4="<?php echo $_GET['s']; ?>";
}
s.prop5=""
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
s.t()//]]></script>
<!-- End SiteCatalyst code version: H.23.6. -->
 