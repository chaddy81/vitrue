<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Vitrue 3.0
 * @subpackage none
 * Template Name: Home
 */

get_header();

global $post;
$custom = get_post_custom($post->ID);
$large_image = $custom["large_image"][0];
$large_url = $custom["large_url"][0];
$button_url = $custom["button_url"][0];

?>

  <section id="slider">
    <div id="slides">
      <div class="slides_container">
        <div>
          <img src="http://www.vitrue.com/wp-content/uploads/2012/01/CommunityManagerHero1.png" style="margin-left:60px;" />
          <div style="position:absolute;top:50px;left:40px;width:350px;">
            <h3 style="font-weight:bold !important;color:#3b4b79;text-shadow:none;text-transform:none;font-size:18px !important;line-height:26px !important;font-family:'Cabin' !important;">Community Manager of the Year 2012</h3>
            <p style="font-size:15px;color:#414141;line-height:24px;margin-bottom:35px;">Nominate your community manager rockstar for the 2012 Community Manager of the Year award. Finalists win a free trip to the WoMMA Summit in Vegas!</p>

            <a href="http://www.vitrue.com/community-manager-of-the-year"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/button-CMY.png" alt="Vitrue Publisher" style="margin-left:0 !important;margin-top:10px;" /></a>
          </div>
        </div>
        <div>
          <img src="http://www.vitrue.com/wp-content/uploads/2011/11/publisher_hero.png" />
          <div style="position:absolute;top:50px;left:40px;width:330px;">
            <h3 style="font-weight:bold !important;color:#3b4b79;text-shadow:none;text-transform:none;font-size:18px !important;line-height:26px !important;font-family:'Cabin' !important;">The Vitrue Publisher facilitates the best possible two-way conversation.</h3>
            <p style="font-size:15px;color:#414141;line-height:24px;margin-bottom:35px;">Deliver your brand's message directly to your Facebook, Twitter, and Google+ feeds!</p>

            <a href="http://www.vitrue.com/our-platform/publisher"><img src="http://www.vitrue.com/wp-content/uploads/2012/01/button-1.png" alt="Vitrue Publisher" style="margin-left:0 !important;margin-top:10px;" /></a>
          </div>
        </div>
        <!-- <div>
          <img src="<?php //bloginfo('template_directory'); ?>/images/CarouselImages_05.png" />
          <div style="position:absolute;top:50px;left:40px;width:330px;">
            <h3 style="font-family:'Cabin' !important;font-weight:bold !important;color:#3b4b79;text-shadow:none;text-transform:none;font-size:18px !important;line-height:26px !important;">Born for the social age.</h3>
            <p style="font-size:15px;color:#414141;line-height:24px;margin-bottom:35px;">From the start, Vitrue was founded to help brands cultivate their online communities.</p>
            <a href="http://www.vitrue.com/about-us"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/button-vitrue1.png" style="margin-left:0 !important;margin-top:10px;" /></a>
          </div>
        </div> -->
        <div>
          <img src="http://www.vitrue.com/wp-content/uploads/2012/02/Analytics_Hero_Page.png" style="margin-left:110px;margin-top:20px;" />
          <div style="position:absolute;top:50px;left:40px;width:330px;">
            <h3 style="font-family:'Cabin' !important;font-weight:bold !important;color:#3b4b79;text-shadow:none;text-transform:none;font-size:18px !important;line-height:26px !important;">Make Data Driven Decisions Based on Your Social Marketing Efforts</h3>
            <p style="font-size:15px;color:#414141;line-height:24px;margin-bottom:35px;">Measure your social media marketing to gauge how your efforts are resonating with your brand's communities.</p>
            <a href="http://www.vitrue.com/our-platform/analytics"><img src="http://www.vitrue.com/wp-content/uploads/2012/02/analytics_button_09.png" style="margin-left:0 !important;margin-top:10px;" /></a>
          </div>
        </div>
        <div>
          <img src="<?php bloginfo('template_directory'); ?>/images/CarouselImages_07.png" />
          <div style="position:absolute;top:50px;left:40px;width:330px;">
            <h3 style="font-family:'Cabin' !important;font-weight:bold !important;color:#3b4b79;text-shadow:none;text-transform:none;font-size:18px !important;line-height:26px !important;">Custom tailoring your Facebook presence has never been easier.</h3>
            <p style="font-size:15px;color:#414141;line-height:24px;margin-bottom:35px;">Vitrue Tabs' custom apps help brands engage with consumers online.</p>
            <a href="http://www.vitrue.com/our-platform/tabs"><img src="http://www.vitrue.com/wp-content/uploads/2012/01/button-tabs.png" style="margin-left:0 !important;margin-top:10px;" /></a>
          </div>
        </div>
        
      </div>
      <a href="#" class="prev"><img src="<?php bloginfo('template_directory'); ?>/images/btn_prev.png" /></a>
      <a href="#" class="next"><img src="<?php bloginfo('template_directory'); ?>/images/btn_next.png" /></a>
    </div>
  </section>
  <section id="content_mid">
  <div id="container_mid">  
	  <div id="content" role="main">
      <section id="main_content">
        <div id="marketing">
          <h2 style="font-size:24px !important;color:#fff;">harness the power of social media with vitrue.</h2>
  		    <p style="font-size:15px;color:#eeff83;">Our solutions will help you utilize social communities for business with the Vitrue SRM.</p>
          <a href="http://vitrue.com/our-platform"><img src="<?php bloginfo('template_directory'); ?>/images/btn_read_more.png" /></a>
        </div>
        <div id="news">
          <h4>Vitrue news</h4>
          <div class="news_container">             
            <?php 
				  		query_posts( 'category_name=press-coverage&posts_per_page=5' );
						
				  		while ( have_posts() ) : the_post();
								echo '<div>';
								the_content();
								echo '</div>';
						  endwhile; 
            ?>
                  
          </div>
          <div id="news_nav_back">
            <a href="#" class="prev"><img src="<?php bloginfo('template_directory'); ?>/images/news_nav_prev.jpg" /></a>
            <a href="#" class="next"><img src="<?php bloginfo('template_directory'); ?>/images/news_nav_next.jpg" /></a>
          </div>
        </div>
        <a href="http://developers.facebook.com/preferreddevelopers/#Vitrue" id="news_pdc"><img src="<?php bloginfo('template_directory'); ?>/images/pdc_news.png" /></a>
		  </section>
      <aside id="sidebar">
        <a class="demo" href="http://vitrue.com/request-a-demo">sign up for a <br /> <span class="large_green">live demo</span><img src="<?php bloginfo('template_directory'); ?>/images/demo_arrow.png" /></a>
        <p id="fans_served">Social Connections <br /> 
		      <span class="large_grey">
    			<?php
    				$curl_handle=curl_init();
    				curl_setopt($curl_handle,CURLOPT_URL,'http://vitruefancounter.dev1.atl.vitrue.com/total');
    				curl_exec($curl_handle);
    				curl_close($curl_handle);
    			?>
		      </span>
        </p>
      </aside>
      <section id="clients">
      	<h2>Clients we have helped</h2>
          <p>We know how to make social media work for the world’s most trusted brands and their partners.</p>
          <img src="http://www.vitrue.com/wp-content/uploads/2012/02/HP-Image-1.png" />
          <a href="http://vitrue.com/what-we-do/who-weve-helped"></a>
      </section>
      <section id="outtakes">
        <div id="outtake_1" class="clearfix">
          <?php
		  			$args = array( 'post_type' => 'custom-one', 'posts_per_page' => 1, 'orderby' => 'date' );
		  			$loop = new WP_Query( $args );
		 
		  			while ( $loop->have_posts() ) : $loop->the_post();
							global $post;
							$custom = get_post_custom($post->ID);
							$custom_url_one = $custom["custom_url_one"][0];
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                              
            	<img src="<?php echo $image[0]; ?>" align="left" style="width:193px;" />
              <h3><?php the_title(); ?></h3>
              <p><?php the_content(); ?></p>
              <a href="<?php echo $custom_url_one; ?>">Read More <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" /></a>
            <?php  endwhile; ?>
        </div>
          
        <hr />
        <div id="outtake_2" class="clearfix">
          <?php
		  			$args = array( 'post_type' => 'custom-two', 'posts_per_page' => 1, 'orderby' => 'date' );
		  			$loop = new WP_Query( $args );
		 
		  			while ( $loop->have_posts() ) : $loop->the_post();
							global $post;
							$custom = get_post_custom($post->ID);
							$custom_url_two = $custom["custom_url_two"][0];
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                              
            	<img src="<?php echo $image[0]; ?>" align="left" />
              <h3><?php the_title(); ?></h3>
              <p><?php the_content(); ?></p>
              <a href="<?php echo $custom_url_two; ?>">Read More <img src="<?php bloginfo('template_directory'); ?>/images/arrow.png" /></a>
            <?php  endwhile; ?>
        </div>
      </section>
		<div class="clear"></div>
	</div><!-- #content -->
  <span id="container_end"></span>
</div><!-- #container -->
    
<?php get_footer(); ?>
