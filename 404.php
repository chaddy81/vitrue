<?php
/**
 * The template for displaying 404 pages.
 *
 * @package Vitrue 3.0
 * @subpackage none
 */

get_header();

	global $post;
	$custom = get_post_custom($post->ID);
	$large_image = $custom["large_image"][0];
	$large_url = $custom["large_url"][0];
	$button_text = $custom["large_url"][0];
	$color_scheme = $custom["color_scheme"][0];
	$small_image = $custom["small_image"][0]; ?>
	
	  <section id="slider_nh">
          
      </section>
      <section id="content_mid" class="nh">
		<div id="container_mid">
        
			<div id="content" role="main">
            	<section id="main_content">
                	<h2>Sorry, your page cannot be found.</h2>                   
		</section>
                <?php get_sidebar('product'); ?>
				<div class="clear"></div>
			</div><!-- #content -->
            <span id="container_end"></span>
		</div><!-- #container -->
        

<?php //get_sidebar(); ?>
<?php get_footer(); ?>